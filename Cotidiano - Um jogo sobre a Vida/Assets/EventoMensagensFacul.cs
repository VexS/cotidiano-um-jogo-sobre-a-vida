﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EventoMensagensFacul : MonoBehaviour
{
    public GameObject Ajudou;
    public GameObject Estudou;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Celular.Ajudou == true)
            Ajudou.SetActive(true);

        if (Celular.Estudou == true)
            Estudou.SetActive(true);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;
using TMPro;
using UnityEngine.SceneManagement;

public class Escolher : MonoBehaviour
{
    public Renders renders;
    public GameObject Player;
    public GameObject Lucas;
    public GameObject Julia;

    public GameObject Vitor;

    public string[] Sinopses;

    public GameObject[] Modelos = new GameObject[2];
    public CinemachineVirtualCamera[] Cams = new CinemachineVirtualCamera[2];
    public Button[] Setas = new Button[2];
    public Button _Confirmar;
    public TextMeshProUGUI Sinopse;
    private int _Selection;

    public Renders _Render;
    public RendersFaculdade Renders = null;

    // Start is called before the first frame update
    void Start()
    {
        _Selection = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(_Selection > 1)
        {
            _Selection = 1;
        }
        else if(_Selection < 0)
        {
            _Selection = 0;
        }

        if (Game.brainChange == true)
        {
            InteractionsOFF.TurnOff = true;

            if(SceneManager.GetActiveScene().name == "Noite")
            {
                Vitor.SetActive(false);
            }

            Player.SetActive(false);

            if (SceneManager.GetActiveScene().name == "Faculdade")
            {
                Julia.SetActive(false);
                Lucas.SetActive(false);
            }

            _Confirmar.gameObject.SetActive(true);

            if (SceneManager.GetActiveScene().name != "Faculdade")
            {
                Modelos[0].SetActive(true);
                Modelos[1].SetActive(true);
            }

            Setas[0].gameObject.SetActive(true);
            Setas[1].gameObject.SetActive(true);

            if (_Selection == 0)
            {
                if(SceneManager.GetActiveScene().name == "Faculdade")
                {
                    Modelos[0].SetActive(true);
                    Modelos[1].SetActive(false);
                }

                Cams[0].Priority = 1;
                Cams[1].Priority = 0;
                Sinopse.text = Sinopses[0];
            }
            else if (_Selection == 1)
            {
                if (SceneManager.GetActiveScene().name == "Faculdade")
                {
                    Modelos[0].SetActive(false);
                    Modelos[1].SetActive(true);
                }

                Cams[0].Priority = 0;
                Cams[1].Priority = 1;
                Sinopse.text = Sinopses[1];
            }
        }

        if(Game.brainChange == false)
        {
            _Confirmar.gameObject.SetActive(false);
            Modelos[0].SetActive(false);
            Modelos[1].SetActive(false);
            Setas[0].gameObject.SetActive(false);
            Setas[1].gameObject.SetActive(false);
            Sinopse.text = "";
        }
    }

    public void Um()
    {
        _Selection = 0;
    }

    public void Dois()
    {
        _Selection = 1;
    }

    public void Confirmar()
    {
        Game.brainChange = false;

        if (_Selection == 0)
        {
            if (SceneManager.GetActiveScene().name == "Noite")
            {
                PlayerPrefs.SetInt("Escolha3", 1);
                renders.linha = 1;
            }

            if (SceneManager.GetActiveScene().name != "Faculdade")
            {
                _Render.Um();
            }

            if(SceneManager.GetActiveScene().name == "Faculdade")
            {
                Renders.Ficar();
            }
        }

        else if (_Selection == 1)
        {
            if (SceneManager.GetActiveScene().name == "Noite")
            {
                PlayerPrefs.SetInt("Escolha3", 2);
                renders.linha = 2;
            }

            if (SceneManager.GetActiveScene().name != "Faculdade")
            {
                _Render.Dois();
            }

            if (SceneManager.GetActiveScene().name == "Faculdade")
            {
                Renders.IrEmbora();
            }
        }
    }
}

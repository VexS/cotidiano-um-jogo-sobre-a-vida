﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour
{
	public GameObject CreditosIMG;

	public GameObject[] Sprites;

	public void Start()
	{
		Sprites[0].SetActive(true);
		Sprites[1].SetActive(false);
	}

	public void Update()
	{
		if (Input.anyKeyDown)
		{
			Sprites[0].SetActive(false);
			Sprites[1].SetActive(true);
		}
	}

	public void LoadLevel(string level)
	{
		SceneManager.LoadScene(level);
	}

	public void Sair()
	{
		Application.Quit();
	}

	public void Creditos()
	{
		CreditosIMG.SetActive(true);
	}

	public void VoltarMenu()
	{
		CreditosIMG.SetActive(false);
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour
{
	public GameObject pauseMenu;
	private bool pause;
	private float originalFixedTime;

    // Start is called before the first frame update
    void Start()
    {
		pause = false;
		pauseMenu.SetActive(false);
		originalFixedTime = Time.fixedDeltaTime;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
		{
			pause = !pause;

			if (pause)
			{
				PauseGame();
			}

			else
			{
				unPauseGame();
			}

		}
    }

	public void PauseGame()
	{
		Time.timeScale = 0;
		Time.fixedDeltaTime = 0;

		pauseMenu.SetActive(true);
	}

	public void unPauseGame()
	{
		Time.timeScale = 1.0f;
		Time.fixedDeltaTime = originalFixedTime;

		pauseMenu.SetActive(false);
	}

	public void GoToMenu(string level)
	{
		unPauseGame();
		SceneManager.LoadScene(level);
	}

	public void QuitGame()
	{
		Application.Quit();
		print("saiu");
	}
}

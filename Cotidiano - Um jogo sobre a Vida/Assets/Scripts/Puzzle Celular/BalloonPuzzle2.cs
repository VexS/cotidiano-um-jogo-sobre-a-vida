﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BalloonPuzzle2 : MonoBehaviour, IDragHandler, IEndDragHandler, IPointerDownHandler
{	
	public Transform baloonPlace2;

	private Vector2 initialPosition;

	//private bool locked2;

    public static int contador = 0;

    /// <mecanicaNovaAparecerPalavras>
    public GameObject[] palavras;
    private int gerador;
    private int tempo;
    private int tamanhoDaLista = 4;
    private List<int> listinha = new List<int>() { 0, 1, 2, 3 };
    /// <mecanicaNovaAparecerPalavras>

    private RectTransform rectTransform;

    public AudioClip somPegou;
    public AudioClip somSoltou;

    public AudioSource audioSource;

    private bool sohPraAnim;
    private bool segundaAnimacaoDiferenciada;

    public Animator animEncaixar;
    public Animator animArrastar;

    private void Awake()
    {
        gerador = 0;
        tempo = 0;
        sohPraAnim = false;
        segundaAnimacaoDiferenciada = false;
    }

    private void Start()
	{
		initialPosition = transform.position;
        rectTransform = this.GetComponent<RectTransform>();
        //locked2 = false;
        sohPraAnim = true;
        segundaAnimacaoDiferenciada = true;
    }

    private void Update()
    {
        StartCoroutine("ApareceBaloes");

        if (sohPraAnim == true)
        {
            animEncaixar.SetBool("Desaparece", true);
            segundaAnimacaoDiferenciada = true;
        }

            if (contador >= 4)
        {
            DonePuzzleBalloon2.marcadorToDone2 = true;
            contador = 0;
        }
    }

    void IDragHandler.OnDrag(PointerEventData eventData)
	{
		transform.position = eventData.position;
        rectTransform.localScale = new Vector3(0.3109968f, 0.2365059f, 0.2102067f);
    }

	public void OnEndDrag(PointerEventData eventData)
	{
		if (Mathf.Abs(transform.position.x - baloonPlace2.position.x) <= 50f &&
			Mathf.Abs(transform.position.y - baloonPlace2.position.y) <= 50f)
		{
			transform.position = new Vector2(baloonPlace2.position.x, baloonPlace2.position.y); 
            //locked2 = true;
            contador++;
            rectTransform.localScale = new Vector3(0.2853181f, 0.2169778f, 0.1928502f);
            audioSource.PlayOneShot(somSoltou);
            frasesAparecem();

        }
		else
		{
			transform.position = new Vector2(initialPosition.x, initialPosition.y);
            rectTransform.localScale = new Vector3(0.2853181f, 0.2169778f, 0.1928502f);
            audioSource.PlayOneShot(somSoltou);
        }
	}

    private void frasesAparecem()
    {
        gerador = Random.Range(0, listinha.Count);
        for (int i = 0; i < tamanhoDaLista; i++)
        {
            
            int value = listinha[gerador];
            listinha.Remove(value);
            palavras[value].SetActive(true);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        audioSource.PlayOneShot(somPegou);
    }

    IEnumerator ApareceBaloes()
    {
        if(segundaAnimacaoDiferenciada == true)
        {
            yield return new WaitForSeconds(1f);
            animArrastar.SetBool("Aparecer", true);
        }

        StopCoroutine("ApareceBaloes");
    }
}


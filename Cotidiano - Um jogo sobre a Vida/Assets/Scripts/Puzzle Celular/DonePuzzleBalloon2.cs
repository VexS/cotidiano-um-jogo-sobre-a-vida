﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DonePuzzleBalloon2 : MonoBehaviour
{
    public GameObject Inteiro;
    public GameObject Baloes;

    public GameObject PuzzleBalao;

    public Animator DonePuzzleAnim;

    public static bool marcadorToDone2;
    public static bool PodeRepetir = false;

    public AudioClip terminouPuzzle;

    public AudioSource audioSource;

    public GameObject[] celuzarzinho;

    public GameObject[] fraseTutorialPuzzleComplexo;

    private void Awake()
    {
        marcadorToDone2 = false;    
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (marcadorToDone2 == true)
        {
            StartCoroutine("DonePuzzle");
            PlayerMovement.Block = false;
        }
    }
    IEnumerator DonePuzzle()
    {
        Inteiro.SetActive(true);
        audioSource.PlayOneShot(terminouPuzzle);
        DonePuzzleAnim.SetBool("donePuzzle2", true);
        fraseTutorialPuzzleComplexo[0].SetActive(false);
        fraseTutorialPuzzleComplexo[1].SetActive(false);
        yield return new WaitForSeconds(1.22f);
        celuzarzinho[0].SetActive(true);
        celuzarzinho[1].SetActive(true);
        Baloes.SetActive(false);
        yield return new WaitForSeconds(4.5f);
        DonePuzzleAnim.SetBool("donePuzzle2", false);
        Choices.finalPuzzle = true;
        PodeRepetir = true;
        Inteiro.SetActive(false);
        PuzzleBalao.SetActive(false);
        marcadorToDone2 = false;
        if(this.name == "Puzzle Diferenciado Ajudar")
        {
            Choices._PuzzleBalao2 = true;
        }
        else if(this.name == "Puzzle Diferenciado Ignorar")
        {
            Choices._PuzzleBalao1 = true;
        }
        StopCoroutine("DonePuzzle");
    }
}
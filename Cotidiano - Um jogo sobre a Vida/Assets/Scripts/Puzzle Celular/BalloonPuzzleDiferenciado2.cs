﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BalloonPuzzleDiferenciado2 : MonoBehaviour, IDragHandler, IEndDragHandler, IPointerDownHandler
{

    public Transform baloonPlace2;

    private Vector2 initialPosition;

   // private bool locked2;

    public static int contador = 0;

    /// <mecanicaNovaAparecerPalavras>
    public GameObject[] palavras;
    private int gerador;
    private int tempo;
    private int tamanhoDaLista = 4;
    private List<int> listinha = new List<int>() { 0, 1, 2, 3 };
    /// <mecanicaNovaAparecerPalavras>

    private RectTransform rectTransform;

    public AudioClip somPegou;
    public AudioClip somSoltou;

    public AudioSource audioSource;

    private void Awake()
    {
        gerador = 0;
        tempo = 0;
    }

    private void Start()
    {
       // locked2 = false;
        initialPosition = transform.position;
        rectTransform = this.GetComponent<RectTransform>();
    }

    private void Update()
    {
        if (contador >= 7)
        {
            DonePuzzleBalloon2.marcadorToDone2 = true;
            contador = 0;
        }
    }

    void IDragHandler.OnDrag(PointerEventData eventData)
    {
        transform.position = eventData.position;
        rectTransform.localScale = new Vector3(0.6458266f, 0.4235313f, 0.3084946f);
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (Mathf.Abs(transform.position.x - baloonPlace2.position.x) <= 40f &&
            Mathf.Abs(transform.position.y - baloonPlace2.position.y) <= 20f)
        {
            transform.position = new Vector2(baloonPlace2.position.x, baloonPlace2.position.y);
            //locked2 = true;
            contador++;
            rectTransform.localScale = new Vector3(0.5862091f, 0.3844343f, 0.2800169f);
            audioSource.PlayOneShot(somSoltou);
            frasesAparecem();

        }
        else
        {
            transform.position = new Vector2(initialPosition.x, initialPosition.y);
            rectTransform.localScale = new Vector3(0.5862091f, 0.3844343f, 0.2800169f);
            audioSource.PlayOneShot(somSoltou);
        }
    }

    private void frasesAparecem()
    {
        gerador = Random.Range(0, listinha.Count);
        for (int i = 0; i < tamanhoDaLista; i++)
        {
            int value = listinha[gerador];
            listinha.Remove(value);
            palavras[value].SetActive(true);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        audioSource.PlayOneShot(somPegou);
    }
}

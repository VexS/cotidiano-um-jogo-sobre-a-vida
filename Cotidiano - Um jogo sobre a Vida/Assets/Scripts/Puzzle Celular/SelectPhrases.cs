﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SelectPhrases : MonoBehaviour
{
    public GameObject[] choices;

    public GameObject textBox;

    public static bool choiceMade;

    public void Escolha1()
    {
		textBox.GetComponent<TextMeshProUGUI>().text = "Nem vo";
        choiceMade = true;
    }

    public void Escolha2()
    {
        textBox.GetComponent<TextMeshProUGUI>().text = "Depende do meu irmão";
        choiceMade = true;
    }

    public void Escolha3()
    {
        textBox.GetComponent<TextMeshProUGUI>().text = "Claro que vou!";
        choiceMade = true;
    }

    private void Update()
    {
        if (choiceMade == true)
        {
            choices[0].SetActive(false);
            choices[1].SetActive(false);
            choices[2].SetActive(false);
        }
    }
}

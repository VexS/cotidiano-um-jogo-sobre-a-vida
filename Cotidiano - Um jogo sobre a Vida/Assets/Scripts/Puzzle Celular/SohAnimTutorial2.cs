﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SohAnimTutorial2 : MonoBehaviour
{
    public Animator animEncaixar;
    public Animator animArrastar;

    private bool sohPraAnim;
    private bool segundaAnimacaoDiferenciada;

    private void Awake()
    {
        sohPraAnim = false;
        segundaAnimacaoDiferenciada = false;
    }

    void Start()
    {
        sohPraAnim = true;
    }

    // Update is called once per frame
    void Update()
    {
        StartCoroutine("ApareceBaloes", true);

        if (sohPraAnim == true)
        {
            animEncaixar.SetBool("desapareceR", true);
            segundaAnimacaoDiferenciada = true;
        }
    }

    IEnumerator ApareceBaloes()
    {
        if (segundaAnimacaoDiferenciada == true)
        {
            yield return new WaitForSeconds(1f);
            animArrastar.SetBool("apareceR", true);
        }

        StopCoroutine("ApareceBaloes");
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DonePuzzleBalloon : MonoBehaviour
{
	public GameObject Inteiro;
	public GameObject Baloes;

    public GameObject PuzzleBalao;

    public Animator DonePuzzleAnim;

    public static bool marcadorToDone;
	public static bool PodeRepetir = false;

    public AudioClip terminouPuzzle;

    public AudioSource audioSource;

    private void Awake()
    {
        marcadorToDone = false;
    }

    // Start is called before the first frame update
    void Start()
    {
		
    }

    // Update is called once per frame
    void Update()
    {
        if (marcadorToDone == true)
		{
            StartCoroutine("DonePuzzle");
            PlayerMovement.Block = false;
        }
    }

    IEnumerator DonePuzzle()
    {    
        Inteiro.SetActive(true);
        audioSource.PlayOneShot(terminouPuzzle);
        DonePuzzleAnim.SetBool("donePuzzle", true);
        Baloes.SetActive(false);
        yield return new WaitForSeconds(4.5f);  
        DonePuzzleAnim.SetBool("donePuzzle", false);
        Choices.finalPuzzle = true;
        PodeRepetir = true;
        Inteiro.SetActive(false);
        PuzzleBalao.SetActive(false);
        marcadorToDone = false;
        StopCoroutine("DonePuzzle");
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BalloonPuzzleTutorial2 : MonoBehaviour, IDragHandler, IEndDragHandler, IPointerDownHandler
{
    public Transform baloonPlace;

    private Vector2 initialPosition;

    // private bool locked;

    public static int contadorTutorial = 0;

    private RectTransform rectTransform;

    public AudioClip somPegou;
    public AudioClip somSoltou;

    public AudioSource audioSource;

    public Animator animatorEncaixar;
    public Animator animatorArrastar;

    private bool sohProAnim;
    private bool sohProAnim2;
    private bool segundoAnimTutorial;

    private void Awake()
    {
        sohProAnim = false;
        sohProAnim2 = false;
        segundoAnimTutorial = false;
    }

    private void Start()
    {
        initialPosition = transform.position;
        rectTransform = this.GetComponent<RectTransform>();

        sohProAnim = true;
    }

    private void Update()
    {
        StartCoroutine("ApresenTutorial");

        if (contadorTutorial >= 4)
        {
            DonePuzzleBalloon.marcadorToDone = true;
            contadorTutorial = 0;

        }

        if (sohProAnim == true)
        {
            animatorEncaixar.SetBool("desaparece", true);
            segundoAnimTutorial = true;
        }
    }



    void IDragHandler.OnDrag(PointerEventData eventData)
    {
        transform.position = eventData.position;
        rectTransform.localScale = new Vector3(0.07401112f, 0.07401112f, 0.07401112f);

    }

    public void OnEndDrag(PointerEventData eventData)
    {

        if (Mathf.Abs(transform.position.x - baloonPlace.position.x) <= 250f &&
            Mathf.Abs(transform.position.y - baloonPlace.position.y) <= 80f)
        {
            transform.position = new Vector2(baloonPlace.position.x, baloonPlace.position.y);
            //locked = true;
            contadorTutorial++;
            rectTransform.localScale = new Vector3(0.065019f, 0.065019f, 0.065019f);

            audioSource.PlayOneShot(somSoltou);
        }
        else
        {
            transform.position = new Vector2(initialPosition.x, initialPosition.y);
            rectTransform.localScale = new Vector3(0.065019f, 0.065019f, 0.065019f);
            audioSource.PlayOneShot(somSoltou);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        audioSource.PlayOneShot(somPegou);
    }

    IEnumerator ApresenTutorial()
    {
        if (segundoAnimTutorial == true)
        {
            yield return new WaitForSeconds(1f);
            animatorArrastar.SetBool("podeAparecer", true);
        }

        StopCoroutine("ApresenTutorial");
    }
}

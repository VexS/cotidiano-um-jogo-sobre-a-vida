﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class BalloonPuzzle : MonoBehaviour, IDragHandler, IEndDragHandler, IPointerDownHandler
{
    public Transform baloonPlace;

    private Vector2 initialPosition;

    //private bool locked;

    public static int contador = 0;

    private RectTransform rectTransform;


    public AudioClip somPegou;
    public AudioClip somSoltou;

    public AudioSource audioSource;

    private void Start()
    {
        //locked = false;
        initialPosition = transform.position;
        rectTransform = this.GetComponent<RectTransform>();

    }

    private void Update()
    {


        if (contador >= 4)
        {
            DonePuzzleBalloon.marcadorToDone = true;
            contador = 0;
        }
    }

    

    void IDragHandler.OnDrag(PointerEventData eventData)
    {
        transform.position = eventData.position;
        rectTransform.localScale = new Vector3(0.9f, 0.3f, 1);

    }

    public void OnEndDrag(PointerEventData eventData)
    {

        if (Mathf.Abs(transform.position.x - baloonPlace.position.x) <= 30f &&
            Mathf.Abs(transform.position.y - baloonPlace.position.y) <= 15f)
        {
            transform.position = new Vector2(baloonPlace.position.x, baloonPlace.position.y);
            //locked = true;
            contador++;
            rectTransform.localScale = new Vector3(0.82719f, 0.27573f, 1);

            audioSource.PlayOneShot(somSoltou);
        }
        else
        {
            transform.position = new Vector2(initialPosition.x, initialPosition.y);
            rectTransform.localScale = new Vector3(0.82719f, 0.27573f, 1);
            audioSource.PlayOneShot(somSoltou);
        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        audioSource.PlayOneShot(somPegou);
    }
}
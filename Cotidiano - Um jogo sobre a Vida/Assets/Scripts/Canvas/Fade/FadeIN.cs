﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FadeIN : MonoBehaviour
{
	public Renders renders;

	public GameObject Canvas;
	public Animator anim;
	public string cena;
	public static bool ProxCena;

	AudioSource sonzinho;

	public AudioClip SOMFadeIn;

	public static bool timeskip = false;
    public static bool ParaMusica = false;

	// Start is called before the first frame update
	void Start()
	{
        ProxCena = true;
		sonzinho = GetComponent<AudioSource>();
	}

    private void Update()
    {
        if (timeskip == true)
            StartFadeIN();

        if (ConversaCelularPuzzle.PosAjuda == true)
        {
            anim.SetBool("Fade", false);
        }
	}

	public void PassarCena()
    {
		if (ProxCena == true)
		{
			SceneManager.LoadScene(cena);
			anim.SetBool("FadeIn", false);
			ProxCena = false;
		}
		
		else if(ProxCena == false)
		{
			anim.SetBool("FadeIn", false);
			anim.SetBool("FadeOUT", true);
			anim.SetBool("FadeOUT", false);
		}
    }

	public void StartFadeIN()
	{
		anim.SetBool("FadeIn", true);
		anim.SetBool("FadeOUT", false);
	}

    public void FadeINSemTrasicao()
    {
        anim.SetBool("Fade", true);
        anim.SetBool("FadeOUT", false);
    }

	public void SOMTimeskip()
	{
		sonzinho.PlayOneShot(SOMFadeIn);
	}

    public void DEIXARFALSO()  
    {
        ConversaCelularPuzzle.PosAjuda = true;
    }

    public void JafoiTimeskip()
    {
		if(Celular.Ajudou != false || Celular.Estudou != false)
		{
         Celular.passouTimeSkip = true;
		}
        
		if(SideButton.voltaDormir)
		{
		  Canvas.GetComponent<Renders>().Consequencia();
		  SideButton.voltaDormir = false;
        }
    }

    public void PodeFecharCelular()
    {
        Celular.Respondeu = false;
        Celular.JaRespondeu = true;
        Celular.Apresentacao = false;
    }
}

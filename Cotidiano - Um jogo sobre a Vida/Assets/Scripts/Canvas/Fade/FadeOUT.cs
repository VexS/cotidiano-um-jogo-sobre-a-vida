﻿	using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeOUT : MonoBehaviour
{
	Animator anim;

    // Start is called before the first frame update
    void Start()
    {
		anim = GetComponent<Animator>();

		StartFadeOUT();
    }

	public void StartFadeOUT()
	{
		anim.SetBool("FadeOUT", true);
		anim.SetBool("FadeOUT", false);
	}

	IEnumerator DesligarFadeOUT()
	{
		yield return new WaitForSeconds(6);
		anim.SetBool("FadeOUTTimeSkip", false);
		StopCoroutine("DesligarFadeOUT");
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;
using UnityEngine.Timeline;
using UnityEngine.UI;

public class Objetivos : MonoBehaviour
{
    public PlayableDirector Director;
    Animator animator;
    public static int teclaObjetivo;
    public static int Espairecer;

    public static bool ObjetivoConcluido;
    public static bool CoisasDeDormir;
    public static bool Encher;
    public static int PrepararMarmita;
    public static int TentarDormir;
    public static int Mochila;
    public static int Conversa;

    public static bool Apresentacao;

    public Image[] concluido;
    public GameObject[] coisasDeDormir;

    AudioSource sonzinho;

    public AudioClip quandoAparece;
    public AudioClip quandoConclui;
    public AudioClip mensagemNova;

    public TextMeshProUGUI Interagir;

    private int mostrar;
    public static int mostrar2;
    private int mostrar3;
    private bool trava;
    public static bool bloqueio;

    void Start()
    {
        CoisasDeDormir = false;
        Apresentacao = false;
        ObjetivoConcluido = false;
        Encher = false;
        Mochila = 0;
        teclaObjetivo = 0;
        bloqueio = false;
        PrepararMarmita = 0;
        Conversa = 0;
        TentarDormir = 0;
        trava = false;
        Espairecer = 0;
        mostrar = 0;
        mostrar2 = -1;
        mostrar3 = 0;
        animator = GetComponent<Animator>();
        sonzinho = GetComponent<AudioSource>();

        if(SceneManager.GetActiveScene().name == "Noite")
        {
            AlphaSubindo();
        }
    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.B))
        {

            Espairecer = 4;
        }

        if(CoisasDeDormir == true)
        {
            coisasDeDormir[0].SetActive(true);
            coisasDeDormir[1].SetActive(true);
            coisasDeDormir[2].SetActive(true);
            CoisasDeDormir = false;
        }

        if (SceneManager.GetActiveScene().name == "Madrugada" && Espairecer <= 4)
        {
            Interagir.text = "- Interagir (" + Espairecer + "/ 4)";
        }

        if(mostrar2 == 0)
        {
            Invoke("AlphaSubindo", 0.75f);
            mostrar2 = 1;
        }

        if(Espairecer == 1 && mostrar == 0)
        {
            teclaObjetivo++;
            mostrar = 1;
        }

        if (Espairecer == 4)
        {
            ObjetivoConcluido = true;
            Espairecer = 5;
        }

        if (Espairecer == 6 && mostrar == 1)
        {
            ObjetivoConcluido = true;
            mostrar = 2;
        }


        if (TentarDormir == 1 && mostrar == 2)
        {
            ObjetivoConcluido = true;
            mostrar = 3;
        }

        if (Mochila == 1 && mostrar3 == 0)
        {
            ObjetivoConcluido = true;
            mostrar3 = 1;
        }

        if (Conversa == 1 && mostrar3 == 1)
        {
            ObjetivoConcluido = true;
            mostrar3 = 2;
        }

        if (Apresentacao == false)
        {
            animator.SetBool("Aprese", false);
        }

        if (teclaObjetivo == 1 && Apresentacao == true)
            animator.SetBool("Aprese", true);

        if (PrepararMarmita == 1 && !trava)
        {
            ObjetivoConcluido = true;
            trava = true;
        }

        if (teclaObjetivo == 1 && Apresentacao == false && Celular.WhatsAberto == false && Celular.ConversaAberta == false)
        {
            AlphaSubindo();
            StartCoroutine("EsperaTempo");
        }

        if (teclaObjetivo == 2 && Apresentacao == false && Celular.WhatsAberto == false && Celular.ConversaAberta == false)
        {
            AlphaDescendo();
            StopCoroutine("EsperaTempo");
        }

        if (teclaObjetivo >= 2)
            teclaObjetivo = 0;

        if (Input.GetKeyDown(KeyCode.O) && !bloqueio)
        {
            if (Game._Tutorial == 0)
            {
                Game._Tutorial = 1;
            }

            teclaObjetivo++;
        }

        //APARECE LISTA DE OBJETIVOS QUANDO ALGUM OBJETIVO É CONCLUIDO
        if (ObjetivoConcluido == true)
        {
            AlphaSubindo();
            Encher = true;
            ObjetivoConcluido = false;
        }

        //ENCHER O RISCO NA HORA CERTA
        if (Encher == true)
        {
            //ENCHE O RISCO
            Invoke("EncherRisco", 1f);
            if (SceneManager.GetActiveScene().name != "Madrugada" && SceneManager.GetActiveScene().name != "Faculdade")
            {
                if (concluido[0].fillAmount >= 1f)
                {
                    concluido[0].fillAmount = 1f;
                    if (SceneManager.GetActiveScene().name != "Noite")
                    {
                        Director.Play();
                    }
                    Encher = false;
                }
            }

            if (SceneManager.GetActiveScene().name == "Madrugada")
            {
                if (concluido[0].fillAmount >= 1f)
                {
                    concluido[0].fillAmount = 1f;
                }

                if (concluido[1].fillAmount >= 1f)
                {
                    concluido[1].fillAmount = 1f;

                    if (Espairecer < 7)
                    {
                        Encher = false;
                    }

                    if (Espairecer < 6)
                    {
                        Espairecer = 6;
                    }
                }

                if (concluido[2].fillAmount >= 1f)
                {
                    concluido[2].fillAmount = 1f;

                    if (PrepararMarmita <= 1)
                    {
                        Encher = false;
                        PrepararMarmita = 2;
                    }
                }

                if(concluido[3].fillAmount >= 1f)
                {
                    concluido[3].fillAmount = 1f;

                    if (TentarDormir == 1)
                    {
                        Encher = false;
                        TentarDormir = 2;
                    }
                }
            }

            if(SceneManager.GetActiveScene().name == "Faculdade")
            {
                if (Mochila == 1)
                {
                    if (concluido[0].fillAmount >= 1f)
                    {
                        concluido[0].fillAmount = 1f;
                        Encher = false;
                    }
                }

                if(Conversa == 1)
                {
                    if (concluido[1].fillAmount >= 1f)
                    {
                        concluido[1].fillAmount = 1f;
                        Encher = false;
                        Conversa = 2;
                    }
                }
            }

        }
    }

    //FAZER OS OBJETIVOS SUMIR
    public void AlphaDescendo()
    {
        if (SceneManager.GetActiveScene().name == "Madrugada")
        {
            if(Espairecer <= 5)
            {
                animator.SetBool("Madrugada", false);
            }

            else if (Espairecer == 7 && PrepararMarmita <= 2)
            {
                animator.SetBool("Marmita", false);

                if(PrepararMarmita == 2)
                {
                    Invoke("Ultimo", 1f);
                }
            }

            else if (PrepararMarmita == 3 && TentarDormir <= 2)
            {
                animator.SetBool("Dormir", false);
                Debug.Log("Descendo");
            }

            if(Espairecer == 6)
            {
                Invoke("Prox", 1f);
            }
        }

        else if (SceneManager.GetActiveScene().name == "Faculdade")
        {
            if (Mochila <= 1)
            {
                animator.SetBool("Mochila", false);

                if (Mochila == 1)
                {
                    Invoke("Conversar", 1f);
                }
            }

            else if (Mochila == 2 && Conversa <= 2)
            {
                animator.SetBool("Conversar", false);
            }
        }

        else if (SceneManager.GetActiveScene().name != "Madrugada" )
        {
            animator.SetBool("ObjAlpha", false);
        }

        StopCoroutine("EsperaTempo");
    }

    //FAZER OS OBJETIVOS APARECER
    public void AlphaSubindo()
    {
        if (SceneManager.GetActiveScene().name == "Madrugada")
        {
            if (Espairecer <= 5)
            {
                animator.SetBool("Madrugada", true);
                StartCoroutine("EsperaTempo");
            }

            else if (Espairecer == 7 && PrepararMarmita <= 1)
            {
                animator.SetBool("Marmita", true);
                StartCoroutine("EsperaTempo");
            }

            else if (PrepararMarmita == 3 && TentarDormir <= 1)
            {
                Debug.Log("Subindo");
                animator.SetBool("Dormir", true);
                StartCoroutine("EsperaTempo");
            }
        }

        else if(SceneManager.GetActiveScene().name == "Faculdade")
        {
            if (Mochila <= 1)
            {
                animator.SetBool("Mochila", true);
                StartCoroutine("EsperaTempo");
            }

            else if (Mochila == 2 && Conversa <= 1)
            {
                animator.SetBool("Conversar", true);
                StartCoroutine("EsperaTempo");
            }
        }
        else
        {
            animator.SetBool("ObjAlpha", true);
            StartCoroutine("EsperaTempo");
        }
    }

    //TEMPO PARA OS OBJETIVOS SUMIREM

    IEnumerator EsperaTempo()
    {
        yield return new WaitForSeconds(3f);

        if (SceneManager.GetActiveScene().name == "Madrugada")
        {
            if (Espairecer <= 4)
            {
                animator.SetBool("Madrugada", true);
            }

            else if (PrepararMarmita <= 1 && Espairecer == 7)
            {
                animator.SetBool("Marmita", true);
            }

            else if (PrepararMarmita == 3 && TentarDormir <= 1)
            {
                animator.SetBool("Dormir", true);
            }

            teclaObjetivo = 0;
            AlphaDescendo();
        }

        else if (SceneManager.GetActiveScene().name == "Faculdade")
        {
            if (Mochila <= 1)
            {
                animator.SetBool("Mochila", true);
            }

            else if (Mochila == 2 && Conversa <= 1)
            {
                animator.SetBool("Conversar", true);
            }

            teclaObjetivo = 0;
            AlphaDescendo();
        }

        else if (SceneManager.GetActiveScene().name != "Madrugada")
        {
            animator.SetBool("ObjAlpha", false);
            teclaObjetivo = 0;
            AlphaDescendo();
        }
    }

    public void SonzinhoAparecendo()
    {
        sonzinho.PlayOneShot(quandoAparece);

        // Quando apertar O e tocar o som da tela ela desliga a bool abaixo para a tela sumir
        if(SceneManager.GetActiveScene().name == "Madrugada")
        {
            animator.SetBool("Madrugada", false);
        }
    }

    public void SOMMensagemNova()
    {
        sonzinho.PlayOneShot(mensagemNova);
    }

    public void EncherRisco()
    {
        if (SceneManager.GetActiveScene().name == "Madrugada")
        {
            if (Espairecer <= 6)
            {
                concluido[0].fillAmount += 0.05f * Time.deltaTime * 17.5f;
                concluido[1].fillAmount += 0.05f * Time.deltaTime * 17.5f;
            }

            else if(Espairecer == 7 && PrepararMarmita == 1)
            {
                concluido[2].fillAmount += 0.05f * Time.deltaTime * 17.5f;
            }

            else if (Espairecer == 7 && PrepararMarmita == 3 && TentarDormir == 1)
            {
                concluido[3].fillAmount += 0.05f * Time.deltaTime * 17.5f;
            }
        }

        else if (SceneManager.GetActiveScene().name == "Faculdade")
        {
            if (Mochila <= 1)
            {
                concluido[0].fillAmount += 0.05f * Time.deltaTime * 17.5f;
            }

            else if(Mochila == 2 && Conversa <= 1)
            {
                concluido[1].fillAmount += 0.05f * Time.deltaTime * 17.5f;
            }
        }
        else
            concluido[0].fillAmount += 0.05f * Time.deltaTime * 17.5f;
    }

	public void SOMQuandoConclui()
	{
        if (Encher == true)
		    sonzinho.PlayOneShot(quandoConclui);
	}

    public void Apresentar()
    {
        animator.SetBool("Madrugada", true);
    }

    public void Prox()
    {
        Espairecer = 7;
        AlphaSubindo();
    }

    public void Ultimo()
    {
        PrepararMarmita = 3;
        AlphaSubindo();
    }

    public void Conversar()
    {
        Mochila = 2;
        AlphaSubindo();
    }
}

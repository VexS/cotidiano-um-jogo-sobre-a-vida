﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Frases : MonoBehaviour
{
    public AudioSource PensamentosSFX;
    public AudioClip[] audio_Pensamentos;

    public TextMeshProUGUI Pensamentos;
    public List<string> FrasesDePensamento = new List<string>();
    public List<string> FrasesDePensamento2 = new List<string>();

    public static int Interromper;
    public static bool Parar;

    [SerializeField] public static int frases;

    public float tempoDeExibição;
    public float tempoDeEspera;
    int i;
    IEnumerator coroutine;

    // Start is called before the first frame update
    void Start()
    {
        Interromper = 0;
        Parar = false;
        frases = 0;
        i = 0;
        FrasesDePensamento.Add("Eu só quero chegar em casa, estudar para a prova de amanhã e dormir.");
        FrasesDePensamento.Add("Será que eu exigi demais do Vitor pedindo para ele arrumar a casa?");

        FrasesDePensamento2.Add("Será que fui muito chato deixando de responder o Lucas?");
        FrasesDePensamento2.Add("Não posso procastinar por muito tempo, tenho que estudar.");
        FrasesDePensamento2.Add("Já deixei de responder o Lucas, não posso deixar de estudar também!");
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1)) frases = 3;

        if (frases == 1) StartCoroutine("ExibirPensamento");
        else if (frases == 3) StartCoroutine("ExibirPensamento2");

        if(Parar)
        {
            StopAllCoroutines();
            Pensamentos.text = "";
        }
    }

    IEnumerator ExibirPensamento() //Exibe a frase de pensamento
    {
        frases = 0;
        i = Random.Range(0, 1);
        yield return new WaitForSeconds(4f);
        yield return new WaitUntil(() => Interromper != 1);
        PensamentosSFX.PlayOneShot(audio_Pensamentos[2]);
        Pensamentos.text = "Ainda é quarta-feira...Estou exausto.";
        yield return new WaitForSeconds(3.75f);
        yield return new WaitUntil(() => Interromper != 1);
        Pensamentos.text = null;
        yield return new WaitForSeconds(1.5f);
        yield return new WaitUntil(() => Interromper != 1);
        Pensamentos.text = "Tenho que conversar com meu chefe sobre essas horas extras...";
        yield return new WaitForSeconds(3.75f);
        Pensamentos.text = null;
        yield return new WaitForSeconds(tempoDeEspera);
        yield return new WaitUntil(() => Interromper != 1);
        PensamentosSFX.PlayOneShot(audio_Pensamentos[i]);
        Pensamentos.text = FrasesDePensamento[i];
        yield return new WaitForSeconds(tempoDeExibição);
        Pensamentos.text = null;

        if (i == 0)
        {
            i = 1;
        }
        else if (i == 1)
        {
            i = 0;
        }
        yield return new WaitForSeconds(tempoDeEspera);
        yield return new WaitUntil(() => Interromper != 1);

        if (i == 0)
        {
            PensamentosSFX.PlayOneShot(audio_Pensamentos[0]);
        }
        else if(i == 1)
        {
            PensamentosSFX.PlayOneShot(audio_Pensamentos[1]);
        }

        Pensamentos.text = FrasesDePensamento[i];
        yield return new WaitForSeconds(tempoDeExibição);
        Pensamentos.text = null;
        frases = 0;
        StopCoroutine("ExibirPensamento");
    }
    IEnumerator ExibirPensamento2() //Exibe a frase de pensamento
    {
        frases = 0;
        yield return new WaitForSeconds(4f);
        yield return new WaitUntil(() => Interromper != 1);
        PensamentosSFX.PlayOneShot(audio_Pensamentos[0]);
        Pensamentos.text = FrasesDePensamento2[0];
        yield return new WaitForSeconds(5f);
        Pensamentos.text = null;
        yield return new WaitForSeconds(3f);
        yield return new WaitUntil(() => Interromper != 1);
        PensamentosSFX.PlayOneShot(audio_Pensamentos[1]);
        Pensamentos.text = FrasesDePensamento2[1];
        yield return new WaitForSeconds(tempoDeExibição);
        Pensamentos.text = null;
        yield return new WaitForSeconds(tempoDeEspera);
        PensamentosSFX.PlayOneShot(audio_Pensamentos[2]);
        Pensamentos.text = FrasesDePensamento2[2];
        yield return new WaitUntil(() => Interromper != 1);
        yield return new WaitForSeconds(tempoDeExibição);
        Pensamentos.text = null;
        StopCoroutine("ExibirPensamento2");
    }
}

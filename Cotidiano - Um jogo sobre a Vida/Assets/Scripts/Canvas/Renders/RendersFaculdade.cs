﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;


public class RendersFaculdade : MonoBehaviour
{
    private int i;

    private int i2;

    private int clicks;

    private bool inicio;

    public static bool Conversa;

    public static bool Trabalho;

    public static bool FicarNaSala;

    public static bool manter;

    public GameObject Fade;

    public AudioSource RendersSFX;

    public TextMeshProUGUI Legenda;

    public Image[] inicioFaculdade;

    public Image[] inicioDaConversa1;

    public string[] Legenda_ConversaInicio1;

    public Image[] IrEmbora1;

    public string[] Legenda_IrEmbora1;

    public Image[] inicioDaConversa2;

    public string[] Legenda_ConversaInicio2;

    public Image[] IrEmbora2;

    public string[] Legenda_IrEmbora2;

    public Image[] inicioDaConversa3;

    public string[] Legenda_Conversainicio3;

    public Image[] IrEmbora3;

    public string[] Legenda_IrEmbora3;

    public Image[] Conversar;

    public string[] Legenda_Conversa;

    public AudioSource RenderSFX;

    public AudioClip[] InicioAjuda;

    public AudioClip[] InicioSemAjuda;

    public AudioClip[] ConversaAmigos;

    public AudioClip[] TrabalhoComAjuda;

    public AudioClip[] TrabalhoSemAjuda;

    private bool travaAudio;

    void Start()
    {
        i = 0;
        i2 = 0;
        clicks = 0;
        Objetivos.bloqueio = true;
        inicio = true;
        manter = true;
        Conversa = false;
        Trabalho = false;
        FicarNaSala = false;
        travaAudio = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (inicio)
        {
            Objetivos.bloqueio = true;
            SimpleSequence(inicioFaculdade);
        }

        if(Conversa)
        {
            if(PlayerPrefs.GetInt("Ajudei") == 2)
            {
                ImageSequence(inicioDaConversa1, InicioAjuda);
            }

            else if (PlayerPrefs.GetInt("Ajudei") == 1 && canPressGH.pontos >= 5)
            {
                ImageSequence(inicioDaConversa2, InicioSemAjuda);
            }

            else if (PlayerPrefs.GetInt("Ajudei") == 1 && canPressGH.pontos <= 5)
            {
                ImageSequence(inicioDaConversa3, InicioSemAjuda);
            }
        }

        if (FicarNaSala)
        {
            ImageSequence(Conversar, ConversaAmigos);
        }

        if (Trabalho)
        {
            if (PlayerPrefs.GetInt("Ajudei") == 2)
            {
                ImageSequence(IrEmbora1, TrabalhoComAjuda);
            }

            else if (PlayerPrefs.GetInt("Ajudei") == 1 && canPressGH.pontos >= 5)
            {
                ImageSequence(IrEmbora2, TrabalhoSemAjuda);
            }

            else if (PlayerPrefs.GetInt("Ajudei") == 1 && canPressGH.pontos <= 5)
            {
                ImageSequence(IrEmbora3, TrabalhoSemAjuda);
            }
        }
    }

     void ImageSequence(Image[] Renders, AudioClip[] Dub)
    {
        if(clicks >= Renders.Length)
        {
            clicks = Renders.Length;
        }

        if(i == 0 && !travaAudio) //Liga o primeiro frame, o restante é ligado pelo clique (Ver abaixo)
        {
           Renders[0].enabled = true;
           RenderSFX.Stop();
           RenderSFX.PlayOneShot(Dub[0]);
           travaAudio = true;
        }

        if (Input.GetMouseButtonDown(0) && i < Renders.Length) // Avançar com clique Esquerdo
        {
            clicks += 1;
            i += 1;
            i2 += 1;

            if(i < Renders.Length)
            {
                Renders[i].enabled = true;
                RenderSFX.Stop();

                if(Renders[i].gameObject.tag == "Sem Som")
                {
                    i2 -= 1;
                }

                if (Renders[i].gameObject.tag != "Sem Som")
                {
                    RenderSFX.PlayOneShot(Dub[i2]);
                }
            }

            Renders[i - 1].enabled = false;
        }

        else if (Input.GetMouseButtonDown(1) && i > 0) // Voltar com clique direito
        {
            clicks -= 1;
            i -= 1;
            i2 -= 1;

            if(i < Renders.Length)
            {
                Renders[i].enabled = true;

                if (Renders[i].gameObject.tag == "Sem Som")
                {
                    i2 += 1;
                }

                if (Renders[i].gameObject.tag != "Sem Som")
                {
                    RenderSFX.PlayOneShot(Dub[i]);
                }
            }

            Renders[i + 1].enabled = false;
        }

        if(i > Renders.Length - 1) // Fim dos Renders
        {
            if(manter)
            {
                Renders[i - 1].enabled = true; // Mantêm o último frame ligado
            }

            if(Conversa)
            {
                Game.brainChange = true;
                i = 0;
                i2 = 0;
                clicks = 0;
                Conversa = false;
            }

            if(FicarNaSala)
            {
                FadeIN.timeskip = true;
                FicarNaSala = false;
            }

            if(Trabalho)
            {
                FadeIN.timeskip = true;
                Trabalho = false;
            }

            Legenda.text = "";
            travaAudio = false;
        }
    }

    void SimpleSequence(Image[] Renders)
    {
        if (clicks >= Renders.Length)
        {
            clicks = Renders.Length;
        }

        if (i == 0) //Liga o primeiro frame, o restante é ligado pelo clique (Ver abaixo)
        {
            Renders[0].enabled = true;
        }

        if (Input.GetMouseButtonDown(0) && i < Renders.Length) // Avançar com clique Esquerdo
        {
            clicks += 1;
            i += 1;

            if (i < Renders.Length)
            {
                Renders[i].enabled = true;
            }

            Renders[i - 1].enabled = false;
        }

        else if (Input.GetMouseButtonDown(1) && i > 0) // Voltar com clique direito
        {
            clicks -= 1;
            i -= 1;

            if (i < Renders.Length)
            {
                Renders[i].enabled = true;
            }

            Renders[i + 1].enabled = false;
        }

        if (i > Renders.Length - 1) // Fim dos Renders
        {
            if (manter)
            {
                Renders[i - 1].enabled = true; // Mantêm o último frame ligado
            }

            if (inicio)
            {
                // Inicia o Fade padrão que liga a prova em seguida
                Fade.GetComponent<Fade>().IN();
                i = 0;
                clicks = 0;
                inicio = false;
            }
        }
    }

    public void Ficar()
    {
        i = 0;
        clicks = 0;
        FicarNaSala = true;
    }

    public void IrEmbora()
    {
        i = 0;
        clicks = 0;
        Trabalho = true;
    }
}

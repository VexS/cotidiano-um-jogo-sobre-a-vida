﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class Renders : MonoBehaviour
{
    public Image[] Introducao;

    public Image[] EscolhaUm;

    public AudioClip[] audio_EscolhaUm;

    public string[] Legendas_EscolhaUm;

    public Image[] EscolhaDois;

    public AudioClip[] audio_EscolhaDois;

    public string[] Legendas_EscolhaDois;

    public AudioClip[] audio_RendersMadru;

    public string[] Legendas_Madru;

    public AudioClip[] audio_Inicio;

    public Image[] ConversaInicio;

    public string[] Legendas_ConversaInicio;

    public AudioClip[] SFX;

    public AudioSource _Audio;

    public AudioSource _audinho;

    public TextMeshProUGUI Legenda;

    public FadeIN Fade;

    public Animator mButtons;

    public static bool inicio;

    private bool Intro;

    private bool trava2;

    private bool trava3;

    private bool trava;

    private bool manter;

    private bool _Play;
    Scene scene;
    [HideInInspector] public int linha;
    [HideInInspector] public int i;
    [HideInInspector] public int i2;
    [HideInInspector] public int clicks;
    int cliqueEsq;
    int cliqueDir;

    // Start is called before the first frame update
    void Awake()
    {
        manter = false;
        inicio = false;
        Intro = false;
        trava = false;
        trava2 = false;
        trava3 = false;
        _Play = false;

        clicks = 0;
        cliqueDir = 0;
        cliqueEsq = 0;
        i = 0;
        i2 = 0;
        linha = 0;

        scene = SceneManager.GetActiveScene();
        
    }

    private void Start()
    {
        if(SceneManager.GetActiveScene().name == "Noite")
        {
            manter = true;
        }
    }

    private void Update()
    {
        if(cliqueEsq >= 1)
        {
            cliqueEsq = 1;
        }
        else if(cliqueDir >= 1)
        {
            cliqueDir = 1;
        }

        if (SceneManager.GetActiveScene().name == "Corredor" && !Intro)
        {
            SimpleSequence(Introducao);

           
        }

        if (SceneManager.GetActiveScene().name == "Noite" && inicio)
        {
            ImageSequence(ConversaInicio, audio_Inicio, Legendas_ConversaInicio);
        }

        if (linha == 1)
        {
             ImageSequence(EscolhaUm, audio_EscolhaUm, Legendas_EscolhaUm);
        }
        else if (linha == 2)
        {
            if (SceneManager.GetActiveScene().name == "Madrugada")
            {
                SimpleSequence(EscolhaDois);
            }

            else ImageSequence(EscolhaDois, audio_EscolhaDois, Legendas_EscolhaDois);
        }
    }

    public void Consequencia()
    {
        if (scene.name == "Apartamento" && PlayerPrefs.GetString("Escolha1") == "Brigar")
        {
            linha = 1;
            manter = true;
        }
        else if (scene.name == "Apartamento" && PlayerPrefs.GetString("Escolha1") == "Ignorar")
        {
            linha = 2;
            manter = true;
        }
    }

    public void Um()
    {
        if (scene.name == "Corredor")
        {
            PlayerPrefs.SetString("Escolha1", "Brigar");
           // _Animator.SetBool("Fade", true);
            linha = 1;
        }

        else if (scene.name == "Apartamento" && PlayerPrefs.GetString("Escolha1") == "Brigar") 
        {
           // _Animator.SetBool("Fade", true);
            linha = 1;
            FadeIN.timeskip = true;
            FadeIN.ProxCena = true;
        }
    }

    public void Dois()
    {
        if(scene.name == "Madrugada")
        {
            linha = 2;
        }
        

        if (scene.name == "Corredor")
        {
            PlayerPrefs.SetString("Escolha1", "Ignorar");
            linha = 2;
        }

        else if (scene.name == "Apartamento" && PlayerPrefs.GetString("Escolha1") == "Ignorar")
        {
            linha = 2;
			FadeIN.timeskip = true;
            FadeIN.ProxCena = true;
		}
    }

    void ImageSequence(Image[] Renders, AudioClip[] Clips, string[] Textos)
    {
        if(SceneManager.GetActiveScene().name == "Corredor")
        {
            StartCoroutine("ExibirControles");
        } 
        
        if(i == 0 && !trava2 && SceneManager.GetActiveScene().name != "Noite")
        {
           Renders[0].enabled = true;
            _Audio.clip = Clips[0];
            _Audio.PlayOneShot(Clips[0]);
            Legenda.text = Textos[0];
            trava2 = true;
        }

        else if (i == 0 && !trava3 && SceneManager.GetActiveScene().name == "Noite")
        {
            Renders[0].enabled = true;
            _Audio.clip = Clips[i2];
            _Audio.PlayOneShot(Clips[i2]);
            Legenda.text = Textos[i2];
            trava3 = true;
        }

        if (SceneManager.GetActiveScene().name == "Corredor" && Renders[0].enabled == true && Renders[0].sprite.name == "Cena 1 -1" && _Play == true)
        {
            _Audio.PlayOneShot(SFX[0]);
            _Play = false;
        }

        if (Input.GetMouseButtonDown(0) && i < Renders.Length)
        {
            clicks += 1;
            i += 1;
            i2 += 1;

            if(SceneManager.GetActiveScene().name == "Corredor")
            {
                if(Renders[i].sprite.name == "Cena 1 -15" && linha == 1)
                {
                    _Audio.clip = SFX[1];
                    _Audio.Stop();
                    _Audio.PlayOneShot(SFX[1]);
                    _Play = false;
                }
            
                else if (Renders[i].sprite.name == "Cena 1 -16" && linha == 1)
                {
                    _Audio.clip = SFX[2];
                    _Audio.Stop();
                    _Audio.PlayOneShot(SFX[2]);
                    _Play = false;
                }
            }

            if (i < Renders.Length)
            {
                Renders[i].enabled = true;
                _Audio.Stop();

                if (SceneManager.GetActiveScene().name == "Noite" && Renders[i].gameObject.tag == "Sem Som")
                {
                    i2 -= 1;
                }

                if (SceneManager.GetActiveScene().name == "Noite" && Renders[i].gameObject.tag != "Sem Som")
                {
                    _Audio.PlayOneShot(Clips[i2]);
                    Legenda.text = Textos[i2];
                }

                else if(SceneManager.GetActiveScene().name != "Noite")
                {
                    _Audio.PlayOneShot(Clips[i]);
                    Legenda.text = Textos[i];
                }
            }

            Renders[i - 1].enabled = false;
        }

        else if (Input.GetMouseButtonDown(1) && i > 0)
        {
            clicks -= 1;
            i -= 1;
            i2 -= 1;

            if (SceneManager.GetActiveScene().name == "Corredor")
            {
                if (Renders[i].sprite.name == "Cena 1 -15" && linha == 1)
                {
                    _Audio.clip = SFX[1];
                    _Audio.Stop();
                    _Audio.PlayOneShot(SFX[1]);
                    _Play = false;
                }

                else if (Renders[i].sprite.name == "Cena 1 -16" && linha == 1)
                {
                    _Audio.clip = SFX[2];
                    _Audio.Stop();
                    _Audio.PlayOneShot(SFX[2]);
                    _Play = false;
                }
            }

            if (i < Renders.Length)
            {
                Renders[i].enabled = true;
                _Audio.Stop();

                if (SceneManager.GetActiveScene().name == "Noite" && Renders[i].gameObject.tag == "Sem Som")
                {
                    i2 += 1;
                }

                if (SceneManager.GetActiveScene().name == "Noite" && Renders[i].gameObject.tag != "Sem Som")
                {
                    _Audio.PlayOneShot(Clips[i2]);
                    Legenda.text = Textos[i2];
                }

                else if (SceneManager.GetActiveScene().name != "Noite")
                {
                    _Audio.PlayOneShot(Clips[i]);
                    Legenda.text = Textos[i];
                }
            }

            Renders[i + 1].enabled = false;
        }

        if (i > Renders.Length - 1)
        {
            if (SceneManager.GetActiveScene().name == "Corredor")
            {
                StopCoroutine("ExibirControles");
            }

            if (inicio)
            {
                inicio = false;
                i = 0;
                i2 = 0;
                clicks = 0;
            }

            if(manter && SceneManager.GetActiveScene().name != "Noite")
            {
                Renders[i - 1].enabled = true;
            }

            if (SceneManager.GetActiveScene().name != "Noite")
            {
                // Ligar Fade e passar para a próxima cena
                Fade.StartFadeIN();
                FadeIN.ProxCena = true;
                linha = 0;
            }

            if (SceneManager.GetActiveScene().name == "Noite" && !trava)
            {
                Game.brainChange = true;
                Legenda.text = "";
                trava = true;
                trava3 = false;
            }

            if (SceneManager.GetActiveScene().name == "Noite" && linha == 1 || SceneManager.GetActiveScene().name == "Noite" && linha == 2)
            {
                linha = 0;
                SceneManager.LoadScene("Arigato");
            }
        }
    }

    void SimpleSequence(Image[] Renders)
    {
        if (SceneManager.GetActiveScene().name == "Corredor")
        {
            StartCoroutine("ExibirControles");
        }

        if (i == 0)
        {
            Renders[0].enabled = true;
        }

        if (Input.GetMouseButtonDown(0) && i < Renders.Length)
        {
            clicks += 1;
            i += 1;

            if (i < Renders.Length)
            {
                Renders[i].enabled = true;
            }
            if (SceneManager.GetActiveScene().name == "Madrugada")
            {
                if (Renders[i].sprite.name == "9")
                {
                    _Audio.Stop();
                    _Audio.PlayOneShot(audio_RendersMadru[0]);
                    Legenda.text = Legendas_Madru[0];
                }


                if (Renders[i].sprite.name == "10")
                {
                    Legenda.text = "";
                }

                if (Renders[i].sprite.name == "21")
                {
                    _Audio.Stop();
                    _Audio.PlayOneShot(audio_RendersMadru[1]);
                }

                if (Renders[i].sprite.name == "22")
                {
                    _Audio.Stop();
                    _Audio.PlayOneShot(audio_RendersMadru[2]);
                }

                if (Renders[i].sprite.name == "23")
                {
                    _Audio.Stop();
                    _Audio.PlayOneShot(audio_RendersMadru[3]);
                }

                if (Renders[i].sprite.name == "24")
                {
                    _Audio.Stop();
                    _Audio.PlayOneShot(audio_RendersMadru[4]);
                }

                if (Renders[i].sprite.name == "25")
                {
                    _Audio.Stop();
                    _Audio.PlayOneShot(audio_RendersMadru[5]);
                }

                if (Introducao[i].sprite.name == "4")
                {
                    _audinho.PlayOneShot(SFX[3]);
                }

                if (Introducao[i].sprite.name == "7")
                {
                    _audinho.PlayOneShot(SFX[3]);
                }
            }

            Renders[i - 1].enabled = false;
        }

        else if (Input.GetMouseButtonDown(1) && i > 0)
        {
            clicks -= 1;
            i -= 1;

            if (i < Renders.Length)
            {
                Renders[i].enabled = true;
            }

            if (SceneManager.GetActiveScene().name == "Madrugada")
            {
                if (Renders[i].sprite.name == "9")
                {
                    _Audio.Stop();
                    _Audio.PlayOneShot(audio_RendersMadru[0]);
                    Legenda.text = Legendas_Madru[0];
                }

                if (Renders[i].sprite.name == "10")
                {
                    Legenda.text = "";
                }

                if (Renders[i].sprite.name == "21")
                {
                    _Audio.Stop();
                    _Audio.PlayOneShot(audio_RendersMadru[1]);
                }

                if (Renders[i].sprite.name == "22")
                {
                    _Audio.Stop();
                    _Audio.PlayOneShot(audio_RendersMadru[2]);
                }

                if (Renders[i].sprite.name == "23")
                {
                    _Audio.Stop();
                    _Audio.PlayOneShot(audio_RendersMadru[3]);
                }

                if (Renders[i].sprite.name == "24")
                {
                    _Audio.Stop();
                    _Audio.PlayOneShot(audio_RendersMadru[4]);
                }

                if (Renders[i].sprite.name == "25")
                {
                    _Audio.Stop();
                    _Audio.PlayOneShot(audio_RendersMadru[5]);
                }

                if (Introducao[i].sprite.name == "4")
                {
                    _audinho.PlayOneShot(SFX[3]);
                }

                if (Introducao[i].sprite.name == "7")
                {
                    _audinho.PlayOneShot(SFX[3]);
                }
            }

            Renders[i + 1].enabled = false;
        }

        if (i > Renders.Length - 1)
        {
            if (SceneManager.GetActiveScene().name == "Corredor")
            {
                Game._Tutorial = 0;
                Intro = true;
                i = 0;
                clicks = 0;
                _Audio.Stop();
                _Audio.loop = false;
                _Audio.clip = null;
                StopCoroutine("ExibirControles");
            }

            if(manter && inicio == false)
            {
                Renders[i - 1].enabled = true;
            }

            if (inicio)
            {
                i = 0;
                clicks = 0;
                inicio = false;
            }

            if (SceneManager.GetActiveScene().name == "Madrugada")
            {
                Fade.StartFadeIN();
                FadeIN.ProxCena = true;
                linha = 0;
            }

            if (SceneManager.GetActiveScene().name == "Noite" && !trava)
            {
                Game.brainChange = true;
                trava = true;
            }
        }
    }

    void Controles()
    {
        mButtons.SetBool("Show", true);
    }

    IEnumerator ExibirControles()
    {
        if(SceneManager.GetActiveScene().name == "Corredor")
        {
            while (true)
            {
                yield return new WaitForSeconds(5f);
                if (clicks == 0)
                {
                    Controles();
                    yield return new WaitForSeconds(3f);
                    mButtons.SetBool("Show", false);
                }
            }
        }
    }
}

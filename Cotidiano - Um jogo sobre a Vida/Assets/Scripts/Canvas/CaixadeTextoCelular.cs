﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaixadeTextoCelular : MonoBehaviour
{
	public GameObject CaixadeTexto;
    public GameObject[] opcoesSelect;

	Animator animator;

    // Start is called before the first frame update
    void Start()
    {
		animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(SelectPhrases.choiceMade == true)
        {
            animator.SetBool("Sobe", false);
        }
    }

	public void AbrirCaixa()
	{
		animator.SetBool("Sobe", true);
	}

    public void ButtonSelectAppears()
    {
        opcoesSelect[0].SetActive(true);
        opcoesSelect[1].SetActive(true);
        opcoesSelect[2].SetActive(true);
    }
}

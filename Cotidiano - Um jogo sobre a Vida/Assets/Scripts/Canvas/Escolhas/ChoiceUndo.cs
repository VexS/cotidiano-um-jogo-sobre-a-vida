﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ChoiceUndo : MonoBehaviour, IPointerClickHandler
{
    public Animator[] Renders = new Animator[2];
    public Animator Sinopse;
    public static bool Deselecionar;

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Right)
        {
            if (Renders[0].GetBool("Expandir") == true)
            {
                Renders[0].SetBool("Diminuir", true);
                Renders[0].SetBool("Expandir", false);
                Renders[0].GetComponent<ChoiceEffect>().Aviso.enabled = true;
                Deselecionar = true;
                Renders[0].GetComponent<ChoiceEffect>().infoAnim.SetBool("IN", true);
                Renders[0].GetComponent<ChoiceEffect>().infoAnim.SetBool("OUT", false);
            }
            else if (Renders[1].GetBool("Expandir") == true)
            {
                Renders[1].SetBool("Diminuir", true);
                Renders[1].SetBool("Expandir", false);
                Renders[1].GetComponent<ChoiceEffect>().Aviso.enabled = true;
                Deselecionar = true;
                Renders[1].GetComponent<ChoiceEffect>().info.enabled = true;
                Renders[1].GetComponent<ChoiceEffect>().Aviso.enabled = true;
                Renders[1].GetComponent<ChoiceEffect>().infoAnim.SetBool("IN", true);
                Renders[1].GetComponent<ChoiceEffect>().infoAnim.SetBool("OUT", false);
            }
        }
    }
}

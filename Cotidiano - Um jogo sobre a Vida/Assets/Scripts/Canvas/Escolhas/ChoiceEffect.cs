﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using TMPro;

public class ChoiceEffect : MonoBehaviour, IPointerClickHandler
{
    public Renders Canvas;
    public Choices canvas;
    public Animator anim;
    public TextMeshProUGUI Aviso;
    public string Sinopse;
    public TextMeshProUGUI info;

    public Animator infoAnim;
    private Animator myAnim;

    private int Clicks;

    // Start is called before the first frame update
    void Start()
    {
        Clicks = 0;
        myAnim = GetComponent<Animator>();
    }

    private void Update()
    {
        if(myAnim.GetBool("Diminuir") == true)
        {
            this.Clicks = 0;
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (eventData.button == PointerEventData.InputButton.Left)
        {
            this.Clicks += 1;

            if(this.Clicks == 2)
            {
                if(this.name == "Discutir")
                {
                    Canvas.Um();
                }

                else if(this.name == "Ignorar")
                {
                    Canvas.Dois();
                }

                else if (this.name == "Ajudar")
                {
                    canvas.Ajudar();
                }

                else if (this.name == "Estudar")
                {
                    canvas.Estudo();
                }

                Clicks = 0;
            }
        }
    }

    // Update is called once per frame
    public void Expandir()
    {
        transform.SetSiblingIndex(1);
        Aviso.enabled = false;

        //Animação de Expandir e Diminuir a imagem
        myAnim.SetBool("Expandir", true);
        myAnim.SetBool("Diminuir", false);
        info.text = this.Sinopse;
        infoAnim.SetBool("OUT", true);
        infoAnim.SetBool("IN", false);

        if (anim.GetBool("Expandir") == true)
        {
            anim.SetBool("Diminuir", true);
            anim.SetBool("Expandir", false);
        }
    }
}

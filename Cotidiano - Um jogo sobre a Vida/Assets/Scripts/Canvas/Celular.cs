﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;

public class Celular : MonoBehaviour
{
    public PlayableDirector Vexinho;
    public Game Jogo;

    public GameObject camaInteraction;
    public GameObject camaCam;
    public GameObject[] Wasup;
    public GameObject[] Conversas;
    public GameObject[] ConversaVitor;
    public GameObject MensagemNovaUI;
    public GameObject PuzzleTutorial;
    public GameObject PuzzleComplexoAjudar;
    public GameObject PuzzleComplexoEstudar;
    public GameObject[] InteraçãoDormir;

    public Renders renders;

    Animator anim;

    public static bool WhatsAberto;
    public static bool ConversaAberta;
    public static bool CelularBloqueado;
    public static bool Ajudou;
    public static bool Estudou;
    public static bool passouTimeSkip;
    public static bool JaRespondeu;
    public static bool Respondeu;
    public static bool RespondeVitor;
    public static bool TaConversando;
    public static bool Esc;
    public static bool Apresentacao;

    public AudioClip SomBloqueado;
    private AudioSource sonzinho;

    /// <summary>
    /// ////////////////////// ANIM CHOICES
    /// </summary>
    public Animator escolhas;
    private bool travachoicesAjudar;
    private bool travachoicesEstudar;

    public GameObject playerTransform; // transform do Player pra mudar a posição quando tiver que dormir.

    private void Awake()
    {
        WhatsAberto = false;
        ConversaAberta = false;
        CelularBloqueado = false;
        Ajudou = false;
        Estudou = false;
        passouTimeSkip = false;
        JaRespondeu = false;
        Respondeu = false;
        RespondeVitor = false;
        TaConversando = false;
        Esc = false;
        Apresentacao = false;

        if (SceneManager.GetActiveScene().name == "Apartamento")
        {
            camaCam.SetActive(false);
        }

        if (SceneManager.GetActiveScene().name == "Faculdade")
        {
            //Objetivos.Apresentacao = true;
            //JaRespondeu = true;
            RespondeVitor = true;
        }

        if (SceneManager.GetActiveScene().name == "Corredor")
        {
            CelularBloqueado = true;
        }
    }

    void Start()
    {
        if (this.name != "CelularADM")
        {
            anim = GetComponent<Animator>();
        }
        sonzinho = GetComponent<AudioSource>();

        if (SceneManager.GetActiveScene().name == "Apartamento")
        {
            Apresentacao = true;
        }
    }

    void Update()
    {

        if (SceneManager.GetActiveScene().name == "Faculdade")
        {
            //if (RespondeVitor == true)
                //anim.SetBool("ConversaVitu", true);
        }

        if (JaRespondeu == true)
            Respondeu = false;

        if (JaRespondeu == true)
        {
            MensagemNovaUI.SetActive(true);
        }

        if (this.name == "CONVERSA - Lucas")
        {
            if (Ajudou == true && EventoVezLucas.PuzzleFeito == true && JaRespondeu == false)
                anim.SetBool("Ajudou", true);

            if (Estudou == true && EventoVezLucas.PuzzleFeito == true && JaRespondeu == false)
                anim.SetBool("Estudou", true);

            if (passouTimeSkip == true && JaRespondeu == false)
                anim.SetBool("Passou", true);
        }

        if (this.name == "CelularADM")
        {
            if (ConversaAberta == true)
            {
                Wasup[1].SetActive(false);
            }

            if (WhatsAberto == true)
            {
                Wasup[1].SetActive(true);
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
            VoltarMenu();
    }

    public void SOMCelularBloqueado()
    {
        if (CelularBloqueado == true)
        {
            sonzinho.PlayOneShot(SomBloqueado);
        }
    }

    public void AbrirCelular()
    {
        if (CelularBloqueado == false)
        {
            PlayerMovement.Block = true;
            Wasup[0].SetActive(true);
            WhatsAberto = true;
            Objetivos.Apresentacao = false;
            Objetivos.teclaObjetivo = 0;

            if (SceneManager.GetActiveScene().name == "Apartamento")
            {
                Vexinho.Stop();
                Vexinho.extrapolationMode = DirectorWrapMode.None;
            }

        }
    }

    public void FecharCelular()
    {
        PlayerMovement.Block = false;
        Wasup[0].SetActive(false);
        WhatsAberto = false;

        if (PlayerPrefs.GetInt("Ajudei") == 1 && SceneManager.GetActiveScene().name == "Apartamento")
        {
            Game.cenaLevantar = true;
        }
    }

    public void ConversaLucas0()
    {
        Conversas[0].SetActive(true);
        ConversaAberta = true;
        WhatsAberto = false;

        if (JaRespondeu == false)
            StartCoroutine("TutorialPuzzleCelular");

        Respondeu = true;
    }

    IEnumerator TutorialPuzzleCelular()
    {
        yield return new WaitForSeconds(2f);
        PuzzleTutorial.SetActive(true);
        StopCoroutine("TutorialPuzzleCelular");
    }

    public void ConversaVitor1()
    {
        Conversas[1].SetActive(true);
        ConversaAberta = true;
        WhatsAberto = false;

        if (RespondeVitor == false)
        {
            ConversaVitor[0].SetActive(true);
            print("teste 1");
        }

        else if (RespondeVitor == true)
        {
            ConversaVitor[1].SetActive(true);
            EventoVezVitor.JaRespondeu = true;
            TaConversando = true;
        }
    }
    public void ConversaGrupo2()
    {
        Conversas[2].SetActive(true);
        ConversaAberta = true;
        WhatsAberto = false;
    }

    public void acabouAnimConversaEstudou()
    {
        Wasup[0].SetActive(false);
    }

    public void VoltarMenu()
    {
        if (WhatsAberto == true && Respondeu == false && TaConversando == false && Apresentacao == false)
        {
            FecharCelular();
        }

        if (ConversaAberta == true && Respondeu == false && TaConversando == false)
        {
            Conversas[0].SetActive(false);
            Conversas[1].SetActive(false);
            Conversas[2].SetActive(false);
            ConversaAberta = false;
            WhatsAberto = true;
        }

        if (Respondeu == true || TaConversando == true || Apresentacao == true)
        {
            sonzinho.PlayOneShot(SomBloqueado);
        }
    }

    public void Cutscene()
    {
        renders.Consequencia();
    }

    public void EscolheuAjudar()
    {
        StartCoroutine("waitToChoiceAjudar");
        if (SceneManager.GetActiveScene().name == "Apartamento" && travachoicesAjudar == true)
        {
            InteraçãoDormir[0].GetComponent<SpriteRenderer>().enabled = true;
            InteraçãoDormir[1].SetActive(true);
            escolhas.SetBool("ajudar", true);
            PlayerPrefs.SetInt("Ajudei", 2);       
            PlayerMovement.Block = true;
            playerTransform.SetActive(true);
            camaCam.SetActive(true);
            DonePuzzleBalloon.marcadorToDone = false;
        }
    }

    public void EscolheuEstudar()
    {
        StartCoroutine("waitToChoiceEstudar");
        if (SceneManager.GetActiveScene().name == "Apartamento" && travachoicesEstudar == true)
        {
            escolhas.SetBool("estudar", true);
            PlayerPrefs.SetInt("Ajudei", 1);
            DonePuzzleBalloon.marcadorToDone = false;
        }
    }

    IEnumerator waitToChoiceAjudar()
    {
        travachoicesAjudar = true;
        yield return new WaitForSeconds(1.5f);
        PuzzleComplexoAjudar.SetActive(true);
        Ajudou = true;
        StopCoroutine("waitToChoiceAjudar");
    }

    IEnumerator waitToChoiceEstudar()
    {
        travachoicesEstudar = true;
        yield return new WaitForSeconds(1.5f);
        PuzzleComplexoEstudar.SetActive(true);
        Estudou = true;
        StopCoroutine("waitToChoiceEstudar");
    }
}
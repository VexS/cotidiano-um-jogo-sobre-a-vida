﻿	using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConversaCelularPuzzle : MonoBehaviour
{
	public GameObject[] BaloesLima;
	public GameObject[] BaloesLucas;
    public GameObject[] Ajudou;

	public GameObject PuzzleTutorial2;

	public static bool UltimaFalaLucas;
	public static bool ProxFala;

    public static bool PosAjuda = false;

	public static int ContadorVezLucas = 0;

	//private Celular Desligar;

    public GameObject Choices;

    public GameObject[] Celularzinho;

    private void Awake()
    {
        UltimaFalaLucas = false;
        ProxFala = false;
    }
    // Update is called once per frame
    void Update()
    {
        if (Celular.Ajudou == true)
        {
            Choices.SetActive(false);
        }

        if (Celular.Estudou == true)
        {
            Choices.SetActive(false);
        }

        if (ContadorVezLucas == 1 && DonePuzzleBalloon.marcadorToDone == true)
		{
			BaloesLima[0].SetActive(true);
			EventoVezLucas.BalaoLucas01 = true;
			StartCoroutine("VezdoLucas");
		}

		if (ContadorVezLucas == 2 && DonePuzzleBalloon.marcadorToDone == true)
		{
			BaloesLima[1].SetActive(true);
			StartCoroutine("VezdoLucas2");
            PuzzleTutorial2.SetActive(false);
		}

		/*if (BaloesLima[0].activeInHierarchy == true && ContadorVezLucas == 0)
        {
			Desligar.PuzzleTutorial.SetActive(false);
		}*/

        if (PosAjuda == true)
        {
            Choices.SetActive(false);
		}
    }
    
    //ANTES DA ESCOLHA
	IEnumerator VezdoLucas()
	{
		yield return new WaitForSeconds(3f);
		BaloesLucas[0].SetActive(true);
		
		yield return new WaitForSeconds(2f);
		if (DonePuzzleBalloon.PodeRepetir == true && DonePuzzleBalloon.marcadorToDone == false)
		{
			PuzzleTutorial2.SetActive(true);
			DonePuzzleBalloon.PodeRepetir = false;
        }
  
		StopCoroutine("VezdoLucas");
	}

    IEnumerator VezdoLucas2()
    {
        yield return new WaitForSeconds(3f);
        BaloesLucas[1].SetActive(true);

        yield return new WaitForSeconds(2f);
        Escolhas();

        StopCoroutine("VezdoLucas2");
    }

    public void Escolhas()
    {
      Choices.SetActive(true);
      Celularzinho[0].SetActive(false);
      Celularzinho[1].SetActive(false);
    }

    //DEPOIS DAS ESCOLHAS

}

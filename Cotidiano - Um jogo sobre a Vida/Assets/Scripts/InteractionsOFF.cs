﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractionsOFF : MonoBehaviour
{
    public GameObject[] Interactions;
    public static bool TurnOff;

    private void Start()
    {
        TurnOff = false;
    }
    // Update is called once per frame
    void Update()
    {
        if (TurnOff)
        {
            foreach (GameObject item in Interactions)
            {
                item.SetActive(false);
            }
        }
    }
}

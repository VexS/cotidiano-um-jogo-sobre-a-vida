﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Footsteps : MonoBehaviour
{
    //[FMODUnity.EventRef]
    public const string inputPassos = "event:/Passos";
    FMOD.Studio.EventInstance EventoPassos;
    FMOD.Studio.ParameterInstance AzulejoParameter;
    FMOD.Studio.ParameterInstance MadeiraParameter;

    bool playerMoving;
    public float andarVel;
    private float AzulejoValue;
    private float MadeiraValue;

    void Start()
    {
        EventoPassos = FMODUnity.RuntimeManager.CreateInstance(inputPassos);
        EventoPassos.getParameter("Azulejo", out AzulejoParameter);
        EventoPassos.getParameter("Madeira", out MadeiraParameter);

        InvokeRepeating("ChamaFootsteps", 0, andarVel);
    }

    public void Update()
    {
        AzulejoParameter.setValue(AzulejoValue);
        MadeiraParameter.setValue(MadeiraValue);

        playerMoving = PlayerMovement.Moving;
    }

    void ChamaFootsteps()
    {
        if (playerMoving == true)
        {
            //FMODUnity.RuntimeManager.PlayOneShot(inputsound);
            EventoPassos.start();
        }
    }

    void OnDisable()
    {
        playerMoving = false;
    }

    void OnTriggerStay(Collider MaterialCheck)
    {

        if (MaterialCheck.CompareTag("Azulejo"))
        {
            AzulejoValue = 1f;
            MadeiraValue = 0f;
        }
        if (MaterialCheck.CompareTag("Madeira"))
        {
            AzulejoValue = 0f;
            MadeiraValue = 1f;
        }
    }
}

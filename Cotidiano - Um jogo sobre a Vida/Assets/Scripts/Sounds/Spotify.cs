﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spotify : MonoBehaviour
{
 
    private int musicIndex;
    FMOD.Studio.EventInstance Playlist;
    FMOD.Studio.ParameterInstance PassarMusicaParam;

    public const string EventoMusicas = "event:/Playlist";
    public const string passarMusicaParamString = "PassarMusica";

    private bool isPaused;

    void Start()
    {
        Playlist = FMODUnity.RuntimeManager.CreateInstance(EventoMusicas);
        Playlist.getParameter(passarMusicaParamString, out PassarMusicaParam);
        musicIndex = 1;
    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.R))
        {
            Playlist.start();
        }

        if (Input.GetKeyDown(KeyCode.E))
        {
            if (musicIndex < 2) musicIndex++;
            PassarMusicaParam.setValue(musicIndex);
        }

         if (Input.GetKeyDown(KeyCode.Q))
        {
            if (musicIndex > 1) musicIndex--;
            PassarMusicaParam.setValue(musicIndex);
        }

        if (Input.GetKeyDown(KeyCode.F))
        {
            isPaused = !isPaused;
            Playlist.setPaused(isPaused);
        }
    }
}

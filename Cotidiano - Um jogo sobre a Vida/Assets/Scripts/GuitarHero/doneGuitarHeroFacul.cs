﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class doneGuitarHeroFacul : MonoBehaviour
{

    public Renders _renders;

    public static int exibiRender;
    public TextMeshProUGUI textFrase;
    public Image[] renders;


    // Start is called before the first frame update
    void Start()
    {
        exibiRender = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (exibiRender == 1)
        {
            StartCoroutine("doneRenders");
        }
    }

    IEnumerator doneRenders()
    {
        if (canPressGH.pontos <= 4)
        {
            renders[0].enabled = true;
            textFrase.text = "Que alívio! Até que estava tranquilo, espero que o Lucas tenha ido bem também...";
        }
        else if (canPressGH.pontos > 4)
        {
            renders[1].enabled = true;
            textFrase.text = "Cara... Vou ter que estudar muito pra compensar essa prova. Deu tudo errado!";
        }
        yield return new WaitForSeconds(5f);

        if (canPressGH.pontos <= 4)
        {
            renders[0].enabled = false;
            textFrase.text = "";
        }
        else if (canPressGH.pontos > 4)
        {
            renders[1].enabled = false;
            textFrase.text = "";
        }
        exibiRender = 2;

        StopCoroutine("doneRenders");
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class buttonController : MonoBehaviour
{
    public KeyCode keyToPress;

    public static bool onlyHere;

    public RectTransform rectTransform;

    public Animator animAcertou;

    public Animator animErrou;

    public GameObject textAnimAcertou;
    public GameObject textAnimErrou;
  
    public static bool apertou;

    private AudioSource As;

    public AudioClip somErrou;
    public AudioClip somAcertou;

    // Start is called before the first frame update
    void Start()
    {
        onlyHere = false;
        apertou = false;

        As = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            rectTransform.localScale = new Vector3(0.18f, 0.18f, 0.18f);

            if (apertou == true)
            {
                textAnimAcertou.SetActive(true);
                animAcertou.SetBool("Acertou", true);
                As.PlayOneShot(somAcertou);
                apertou = false;
            }
        }

        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            rectTransform.localScale = new Vector3(0.18f, 0.18f, 0.18f);

            if (apertou == true)
            {
                textAnimAcertou.SetActive(true);
                animAcertou.SetBool("Acertou", true);
                As.PlayOneShot(somAcertou);
                apertou = false;
            }
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            rectTransform.localScale = new Vector3(0.18f, 0.18f, 0.18f);

            if (apertou == true)
            {
                textAnimAcertou.SetActive(true);
                animAcertou.SetBool("Acertou", true);
                As.PlayOneShot(somAcertou);
                apertou = false;
            }
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            rectTransform.localScale = new Vector3(0.18f, 0.18f, 0.18f);

            if (apertou == true)
            {
                textAnimAcertou.SetActive(true);
                animAcertou.SetBool("Acertou", true);
                As.PlayOneShot(somAcertou);
                apertou = false;
            }
        }

        /////////////// abaixo o sprite volta ao normal

        if (Input.GetKeyUp(KeyCode.DownArrow))
        {
            rectTransform.localScale = new Vector3(0.27f, 0.27f, 0.27f);
        }

        if (Input.GetKeyUp(KeyCode.UpArrow))
        {
            rectTransform.localScale = new Vector3(0.27f, 0.27f, 0.27f);
        }

        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            rectTransform.localScale = new Vector3(0.27f, 0.27f, 0.27f);
        }

        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            rectTransform.localScale = new Vector3(0.27f, 0.27f, 0.27f);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "buttom")
        {
            other.gameObject.GetComponent<canPressGH>().canPress = true;
            onlyHere = true;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "buttom")
        {
            other.gameObject.GetComponent<canPressGH>().canPress = false;
            onlyHere = false;
            if (!apertou)
            {
                    canPressGH.errou = true;
            }

            if (canPressGH.errou == true)
            {
                As.PlayOneShot(somErrou);
                textAnimErrou.SetActive(true);
                animErrou.SetBool("Errou", true);
            }
        }
    }
}
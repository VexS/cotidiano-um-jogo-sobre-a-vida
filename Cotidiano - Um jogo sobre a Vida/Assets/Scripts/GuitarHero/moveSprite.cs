﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class moveSprite : MonoBehaviour
{
    public GameObject sprite;
    public RectTransform rectTransform;

    public float speed;

    private void Start()
    {

    }

    private void Update()
    {
        transform.position -= new Vector3(speed * Time.deltaTime, 0f, 0f);

        if(rectTransform.position.x <= -10f)
        {
            sprite.SetActive(false);

            if (SceneManager.GetActiveScene().name == "Apartamento")
            {
                spawnButtons.NumQuadrados -= 1;
            }
            else if (SceneManager.GetActiveScene().name == "Faculdade")
            {
                spawnButtons2.NumQuadrados -= 1;
            }
        }
    }
}

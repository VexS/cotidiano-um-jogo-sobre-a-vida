﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class canPressGH : MonoBehaviour
{
    public bool canPress;

    public KeyCode keyToPress;

   // private int teste = 1;

    public static int pontos = 0;

    public static bool errou;

    // Start is called before the first frame update
    void Start()
    {
        errou = false;
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(keyToPress) && buttonController.onlyHere == true)
        {

            if (canPress)
            {
                buttonController.apertou = true;
                pontos ++;
                buttonController.onlyHere = false;
                if (SceneManager.GetActiveScene().name == "Apartamento")
                {
                    spawnButtons.NumQuadrados -= 1;
                }
                else if(SceneManager.GetActiveScene().name == "Faculdade")
                {
                    spawnButtons2.NumQuadrados -= 1;
                }
                gameObject.SetActive(false);     
            }            
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class spawnButtons : MonoBehaviour
{
    public Game m_Game;

    public GameObject guitarHeroGameObject;
    public Transform localSpawn;

    public GameObject[] objSpawn;


    public static int NumQuadrados = 10;

    private float tempo;

    private int n;
    private int m;

    private int numbers = 10;
    //private int numbersMedium = 10;

    public static bool guitarHero;
    public static bool done;
    public static bool podeFecharGH;

    private Animator doneEstudo;

    public AudioClip terminouEstudo;

    public AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        guitarHero = true;

        podeFecharGH = false;

        if (guitarHero == true)
        {
            StartCoroutine("Spawn");
        }

        doneEstudo = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

        if (NumQuadrados <= 0)
        {
            AnimDone();
            StopCoroutine(Spawn());
            Game.Estudei = true;
            guitarHero = false;
            done = true;
        }

        n = Random.Range(0, 4);
    }

    private IEnumerator Spawn()
    {
        for (int i = 0; i < numbers; i++)
        {
            tempo = Random.Range(2, 4);
            Instantiate(objSpawn[n], localSpawn);
            yield return new WaitForSeconds(tempo);      
        }
    }

    private void AnimDone()
    {
        audioSource.PlayOneShot(terminouEstudo);
        doneEstudo.SetBool("terminouEstudo", true);
    }

    public void TaDa()
    {
        podeFecharGH = true;
    }

    /*private IEnumerator SpawnMedium()
    {
        for (int i = 0; i < numbersMedium; i++)
        {
            tempo = Random.Range(1, 3);
            Instantiate(objSpawnMedium[n], localSpawn);
            yield return new WaitForSeconds(tempo);
        }
        guitarHeroMedium = false;
        doneMedium = true;
    }

    private IEnumerator SpawnHard()
    {
        for (int i = 0; i < numbersHard; i++)
        {
            tempo = Random.Range(1, 2);
            m = Random.Range(0, 3);
            Instantiate(objSpawnToRight[n], Posicoes[m].transform.position,Quaternion.identity);
            Instantiate(objSpawnTopLeft[n], Posicoes[m].transform.position, Quaternion.identity);
            Instantiate(objSpawnTopRight[n], Posicoes[m].transform.position, Quaternion.identity);
            Instantiate(objSpawn[n], Posicoes[m].transform.position, Quaternion.identity);
            yield return new WaitForSeconds(tempo);
        }
        guitarHeroHard = false;
    }*/

}

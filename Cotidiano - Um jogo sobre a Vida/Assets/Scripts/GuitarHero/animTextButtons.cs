﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animTextButtons : MonoBehaviour
{

    public Animator anim;

    public GameObject text;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Acertou()
    {
        anim.SetBool("Acertou", false);
        text.SetActive(false);
        
    }

    public void Errou()
    {
        anim.SetBool("Errou", false);
        text.SetActive(false);
        canPressGH.errou = false;
    }
}

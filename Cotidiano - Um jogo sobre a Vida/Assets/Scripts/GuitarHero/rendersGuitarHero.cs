﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class rendersGuitarHero : MonoBehaviour
{
    public Image[] renders;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("rendersNext");
    }

    // Update is called once per frame
    void Update()
    {

    }

    IEnumerator rendersNext()
    {
        while (true)
        {
            if (SceneManager.GetActiveScene().name == "Apartamento")
            {
                renders[0].enabled = true;
                yield return new WaitForSeconds(4f);
                renders[0].enabled = false;
                renders[1].enabled = true;
                renders[2].enabled = false;

                if (spawnButtons.done)
                {
                    renders[0].enabled = false;
                    renders[1].enabled = false;
                    renders[2].enabled = false;
                    doneGuitarHero.exibiRender = 1;
                    StopCoroutine("rendersNext");
                }
                yield return new WaitForSeconds(4f);
                renders[0].enabled = false;
                renders[1].enabled = false;
                renders[2].enabled = true;

                if (spawnButtons.done)
                {
                    renders[0].enabled = false;
                    renders[1].enabled = false;
                    renders[2].enabled = false;
                    doneGuitarHero.exibiRender = 1;
                    StopCoroutine("rendersNext");
                }
                yield return new WaitForSeconds(4f);
                renders[0].enabled = true;
                renders[1].enabled = false;
                renders[2].enabled = false;

                if (spawnButtons.done)
                {
                    renders[0].enabled = false;
                    renders[1].enabled = false;
                    renders[2].enabled = false;
                    doneGuitarHero.exibiRender = 1;
                    StopCoroutine("rendersNext");
                }
            }

            else if (SceneManager.GetActiveScene().name == "Faculdade")
            {
                renders[0].enabled = true;
                yield return new WaitForSeconds(4f);
                renders[0].enabled = false;
                renders[1].enabled = true;
                renders[2].enabled = false;

                if (spawnButtons2.done)
                {
                    renders[0].enabled = false;
                    renders[1].enabled = false;
                    renders[2].enabled = false;
                    doneGuitarHeroFacul.exibiRender = 1;
                    StopCoroutine("rendersNext");
                }
                yield return new WaitForSeconds(4f);
                renders[0].enabled = false;
                renders[1].enabled = false;
                renders[2].enabled = true;

                if (spawnButtons2.done)
                {
                    renders[0].enabled = false;
                    renders[1].enabled = false;
                    renders[2].enabled = false;
                    doneGuitarHeroFacul.exibiRender = 1;
                    StopCoroutine("rendersNext");
                }
                yield return new WaitForSeconds(4f);
                renders[0].enabled = true;
                renders[1].enabled = false;
                renders[2].enabled = false;

                if (spawnButtons2.done)
                {
                    renders[0].enabled = false;
                    renders[1].enabled = false;
                    renders[2].enabled = false;
                    doneGuitarHeroFacul.exibiRender = 1;
                    StopCoroutine("rendersNext");
                }
            }
        }
    }
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;


public class doneGuitarHero : MonoBehaviour
{

    public Renders _renders;
    private bool exibi;
    public static int exibiRender;
    public TextMeshProUGUI textFrase;
    public Image[] renders;


    // Start is called before the first frame update
    void Start()
    {
        exibi = true;
        exibiRender = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(exibiRender == 1)
        {
            StartCoroutine("doneRenders");
        }
    }

    IEnumerator doneRenders()
    {
        if(canPressGH.pontos <= 4)
        {
            renders[0].enabled = true;
            textFrase.text = "Droga...Não consegui entender direito essa matéria, acho que vou mal nessa prova";
        }
        else if (canPressGH.pontos > 4)
        {
            renders[1].enabled = true;
            textFrase.text = "Acho que consegui entender tudo... espero ir bem na prova";
        }
        yield return new WaitForSeconds(3f);

        if (canPressGH.pontos <= 4)
        {
            renders[0].enabled = false;
            textFrase.text = "";
        }
        else if (canPressGH.pontos > 4)
        {
            renders[1].enabled = false;
            textFrase.text = "";
        }

        exibiRender = 2;
        StopCoroutine("doneRenders");
    }
}

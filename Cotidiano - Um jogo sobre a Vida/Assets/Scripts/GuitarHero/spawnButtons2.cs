﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class spawnButtons2 : MonoBehaviour
{
    public Game m_Game;

    public GameObject guitarHeroGameObject;
    public Transform localSpawn;

    public GameObject[] objSpawn;


    public static int NumQuadrados = 9;

    private float tempo;

    private int n;
    private int m;

    private int numbers = 9;
    //private int numbersMedium = 10;

    public static bool guitarHero;
    public static bool done;
    public static bool podeFecharGH;

    private Animator doneEstudo;

    public AudioClip terminouEstudo;

    public AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        guitarHero = true;

        podeFecharGH = false;

        if (guitarHero == true)
        {
            StartCoroutine("Spawn");
        }

        doneEstudo = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (done)
        {
            if (SceneManager.GetActiveScene().name == "Faculdade")
            {
                m_Game.LigarPlayer();
            }
        }


        if (NumQuadrados <= 0)
        {
            StartCoroutine(AnimDone());
            StopCoroutine(Spawn());
            guitarHero = false;
            done = true;
        }

        n = Random.Range(0, 4);
    }

    private IEnumerator Spawn()
    {
        yield return new WaitForSeconds(.75f);
        for (int i = 0; i < numbers; i++)
        {
            tempo = Random.Range(2, 4);
            Instantiate(objSpawn[n], localSpawn);
            yield return new WaitForSeconds(tempo);
        } 
    }

    private IEnumerator AnimDone()
    {
        audioSource.PlayOneShot(terminouEstudo);
        doneEstudo.SetBool("terminouEstudo2", true);

        yield return new WaitForSeconds(4f);

        podeFecharGH = true;
        guitarHeroGameObject.SetActive(false);
        StopCoroutine(AnimDone());
    }
}

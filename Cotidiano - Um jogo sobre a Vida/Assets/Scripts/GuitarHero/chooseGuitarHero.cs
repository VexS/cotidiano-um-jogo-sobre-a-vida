﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class chooseGuitarHero : MonoBehaviour
{
    public GameObject[] centralButtons;

    // Start is called before the first frame update
    void Start()
    {
        centralButtons[0].SetActive(false);
        centralButtons[1].SetActive(false);

        Choices.guitarHeroDificil = true;
    }

    // Update is called once per frame
    void Update()
    {

        if (Choices.guitarHeroDificil == true)
        {
            centralButtons[0].SetActive(true);
        }
        if (Choices.guitarHeroFacil == true)
        {
            centralButtons[1].SetActive(true);
        }

    }
}

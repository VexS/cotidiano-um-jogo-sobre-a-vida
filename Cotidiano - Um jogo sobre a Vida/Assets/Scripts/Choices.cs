﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Cinemachine;

public class Choices : MonoBehaviour
{
    public CinemachineVirtualCamera portaCam;
    public GameObject[] choiceButtons;
    public GameObject PuzzleBalao;
    public GameObject escolhaChoices;
    public GameObject Wasup;

    int imgIndex;

    public static bool ChooseYourFate;

    public GameObject estudoBotao;

    public static bool _PuzzleBalao1;
    public static bool _PuzzleBalao2;
    public static bool finalPuzzle;

    public static bool guitarHeroFacil;
    public static bool guitarHeroDificil;

    public Animator animConversaLucas;
    public Animator balaoOpaco;
    public Animator apareceBaloesEncaixar;

    
    

    // Start is called before the first frame update
    void Start()
    {
        ChooseYourFate = false;
        finalPuzzle = false;
        _PuzzleBalao1 = false;
        _PuzzleBalao2 = false;
        guitarHeroDificil = false;
        guitarHeroFacil = false;
    }

    // Update is called once per frame
    void Update()
    {

        if (ChooseYourFate)
        {
            choiceButtons[0].SetActive(true);
            choiceButtons[1].SetActive(true);
        }

        if(finalPuzzle == true)
        {
            if (_PuzzleBalao1 == true)
            {
                animConversaLucas.SetBool("Estudou", true);
            }
            else if(_PuzzleBalao2 == true)
            {
                animConversaLucas.SetBool("Ajudou", true);
            }
        }
    }

    public void Estudo()
    {      
        estudoBotao.GetComponent<SpriteRenderer>().enabled = true;
        estudoBotao.GetComponent<CentralButton>().enabled = true;

        escolhaChoices.SetActive(false);
        PuzzleBalao.SetActive(true);
        balaoOpaco.SetBool("toTransparent", true);
        apareceBaloesEncaixar.SetBool("podeAparecer", true);
        
        _PuzzleBalao1 = true;
        guitarHeroFacil = true;
    }

	public void Ajudar()
	{
		escolhaChoices.SetActive(false);
		Wasup.SetActive(true);
        PuzzleBalao.SetActive(true);
        balaoOpaco.SetBool("toTransparent", true);
        apareceBaloesEncaixar.SetBool("podeAparecer", true);
       
        _PuzzleBalao2 = true;
        guitarHeroDificil = true;
    }

    public void Escolhas()
    {
        Game.brainChange = true;
    }

    public void DesligarCam()
    {
        portaCam.enabled = false;
    }
}

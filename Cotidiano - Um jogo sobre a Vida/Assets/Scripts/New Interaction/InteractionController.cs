﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class InteractionController : MonoBehaviour
{
    private bool travaAudio;
    private bool travaEstudo;
    private bool travaFade;
    private bool trava;
    private bool trava2;
    private bool travaEspairecer;
    public static bool trava3;
    public GameObject Fade;
    public Fade p_Fade;
    public TextMeshProUGUI Pensamentos;
    public Renders renders;
    public GameObject[] textCaixa;
    public GameObject[] textPia;
    public GameObject[] textJanela;
    public GameObject[] camCalendario;
    public GameObject[] camLivros;
    public GameObject[] textEspelho;
    public GameObject[] textMural;
    public GameObject[] textVizinho;
    public GameObject[] textContaVizinho;
    public GameObject[] camContaLima;
    public GameObject[] camCarpete;
    public GameObject[] camRetratoMae;
    public GameObject[] textMochila;
    public GameObject[] textCarteira;
    public GameObject[] camAmigos;
    public GameObject[] textAmigos;
    public GameObject[] camMarmita;
    public GameObject[] guitarHero;
    public GameObject backgroundGuitarHero;

    public GameObject[] show;
    public GameObject  allInte;

    public GameObject[] Mochilinha;
    public GameObject[] portaGeladeira;
    public GameObject marmita;

    public Animator anim;
    public Animator animPlayer;
    public Animator animCarpete;
    public Transform player;

    public AudioClip somClick;

    public AudioSource audioSource;

    public Animator apareceGuitarHero;
    public Animator fade;

    public AudioSource PensamentosSFX;
    public AudioClip[] PensamentosInteracoes;

    public float[] TemposDeExibicao;

    // Start is called before the first frame update
    void Start()
    {
        allInte.SetActive(true);
        trava = false;
        trava2 = false;
        trava3 = false;
        travaEspairecer = false;
        travaFade = false;
        travaEstudo = false;
        travaAudio = false;

        if (SceneManager.GetActiveScene().name != "Faculdade")
        {
            player = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        }
    }

    // Update is called once per frame
    private void Update()
    {
        if (SideButton.voltaCaixa == true && !travaAudio)
        {
            Frases.Interromper = 1;
            PensamentosSFX.Stop();
            PensamentosSFX.PlayOneShot(PensamentosInteracoes[0]);
            StartCoroutine("textViewCaixa");
        }

        if (SideButton.voltaCaixa == false)
        {
            StopCoroutine("textViewCaixa");
            //travaAudio = false;
        }

        if (SideButton.voltaPia == true && !travaAudio)
        {
            Frases.Interromper = 1;
            PensamentosSFX.Stop();
            PensamentosSFX.PlayOneShot(PensamentosInteracoes[1]);
            StartCoroutine("textViewPia");
        }

        if (SideButton.voltaPia == false)
        {
            StopCoroutine("textViewPia");
           // travaAudio = false;
        }

        if (SideButton.voltaJanela == true && !travaAudio)
        {
            Frases.Interromper = 1;
            PensamentosSFX.Stop();
            PensamentosSFX.PlayOneShot(PensamentosInteracoes[2]);
            StartCoroutine("textViewJanela");
        }

        if (SideButton.voltaJanela == false)
        {
            StopCoroutine("textViewJanela");
           // travaAudio = false;
        }

        if (SideButton.voltaCalendario == true && !travaAudio)
        {
            Frases.Interromper = 1;
            PensamentosSFX.Stop();
            PensamentosSFX.PlayOneShot(PensamentosInteracoes[5]);
            StartCoroutine("camViewCalendario");
        }

        if (SideButton.voltaCalendario == false)
        {
            StopCoroutine("camViewCalendario");
           // travaAudio = false;
        }

        if (SideButton.voltaLivros == true && !travaAudio)
        {
            Frases.Interromper = 1;
            PensamentosSFX.Stop();
            PensamentosSFX.PlayOneShot(PensamentosInteracoes[6]);
            StartCoroutine("camViewLivros");
        }

        if (SideButton.voltaLivros == false)
        {
            StopCoroutine("camViewLivros");
           // travaAudio = false;
        }

        if (SideButton.voltaEspelho == true && !travaAudio)
        {
            Frases.Interromper = 1;
            PensamentosSFX.Stop();
            PensamentosSFX.PlayOneShot(PensamentosInteracoes[3]);
            StartCoroutine("textViewEspelho");
        }

        if (SideButton.voltaEspelho == false)
        {
            StopCoroutine("textViewEspelho");
          //  travaAudio = false;
        }

        if (SideButton.voltaMural == true && !travaAudio)
        {
            Frases.Interromper = 1;
            PensamentosSFX.Stop();
            PensamentosSFX.PlayOneShot(PensamentosInteracoes[0]);
            StartCoroutine("textViewMural");
        }

        if (SideButton.voltaMural == false)
        {
            StopCoroutine("textViewMural");
           // travaAudio = false;
        }

        if (SideButton.voltaVizinho == true && !travaAudio)
        {
            Frases.Interromper = 1;
            PensamentosSFX.Stop();
            PensamentosSFX.PlayOneShot(PensamentosInteracoes[1]);
            StartCoroutine("textViewVizinho");
        }

        if (SideButton.voltaVizinho == false)
        {
            StopCoroutine("textViewVizinho");
            //travaAudio = false;
        }

        if (SideButton.voltaContaVizinho == true && !travaAudio)
        {
            Frases.Interromper = 1;
            PensamentosSFX.Stop();
            PensamentosSFX.PlayOneShot(PensamentosInteracoes[2]);
            StartCoroutine("textViewContaVizinho");
        }

        if (SideButton.voltaContaVizinho == false)
        {
            StopCoroutine("textViewContaVizinho");
          //  travaAudio = false;
        }

        if (SideButton.voltaContaLima == true && !travaAudio)
        {
            Frases.Interromper = 1;
            PensamentosSFX.Stop();
            PensamentosSFX.PlayOneShot(PensamentosInteracoes[7]);
            StartCoroutine("camViewContaLima");
        }

        if (SideButton.voltaContaLima == false)
        {
            StopCoroutine("camViewContaLima");
           // travaAudio = false;
        }

        if (SideButton.voltaCarpete == true && Collisions.podeAnim == true && !travaAudio)
        {
            StartCoroutine("camViewCarpete");
        }

        if (SideButton.voltaCarpete == false)
        {
            StopCoroutine("camViewCarpete");
        }

        if (SideButton.voltaAbrirPorta == true)
        {
            AbrirPorta();
        }

        if (SideButton.voltaEstudar == true && !travaEstudo)
        {
            StartCoroutine("EstudarGuitarHero");
        }

        if (SideButton.voltaVitorJogando == false)
        {
            StopCoroutine("textViewVitorJogando");
        }

        if (SideButton.voltaVitorJogando == true && !travaAudio)
        {
            Frases.Interromper = 1;
            PensamentosSFX.Stop();
            PensamentosSFX.PlayOneShot(PensamentosInteracoes[4]);
            StartCoroutine("textViewVitorJogando");
        }

        if (SideButton.voltaRetratoMae == true && !travaAudio)
        {
            Frases.Interromper = 1;
            PensamentosSFX.Stop();
            PensamentosSFX.PlayOneShot(PensamentosInteracoes[8]);
            StartCoroutine("textViewRetratoMae");
        }

        if (SideButton.voltaRetratoMae == false)
        {
            StopCoroutine("textViewRetratoMae");
        }

        if (SideButton.voltaRetratoMaeAnim == true)
        {
            StartCoroutine("camViewRetratoMae");
        }

        if (SideButton.voltaRetratoMaeAnim == false)
        {
            StopCoroutine("camViewRetratoMae");
        }

        if (SideButton.voltaMochila == true)
        {
            StartCoroutine("textViewMochila");
        }

        if (SideButton.voltaMochila == false)
        {
            StopCoroutine("textViewMochila");
        }

        if (SideButton.voltaMochilaAnim == true)
        {
            StartCoroutine("animViewMochila");
        }

        if (SideButton.voltaMochilaAnim == false)
        {
            StopCoroutine("animViewMochila");
        }

        if (SideButton.voltaCarteira == true)
        {
            StartCoroutine("textViewCarteira");
        }

        if (SideButton.voltaCarteira == false)
        {
            StopCoroutine("textViewCarteira");
        }

        if (SideButton.voltaAmigos == true)
        {
            StartCoroutine("textViewCamAmigos");
        }

        if (SideButton.voltaAmigos == false)
        {
            StopCoroutine("textViewCamAmigos");
        }

        if (SideButton.voltaMarmita == true)
        {
            StartCoroutine("CamViewMarmita");
        }

        if (SideButton.voltaMarmita == false)
        {
            StopCoroutine("CamViewMarmita");
        }

        if (SideButton.voltaCama3 == true && trava3)
        {
            Objetivos.TentarDormir = 1;
            renders.Dois();
            trava3 = false;
        }

        if (SideButton.voltaAmigos2 == true && Objetivos.Mochila == 0)
        {
            Game.avisoMochila = true;
            SideButton.voltaAmigos2 = false;
        }

        if (SideButton.voltaAmigos2 == true && Objetivos.Mochila == 2)
        {
            allInte.SetActive(false);
            Objetivos.Conversa = 1;
            RendersFaculdade.Conversa = true;
            SideButton.voltaAmigos2 = false;
        }

        if (SideButton.voltaDormir == true && travaFade == false)
        {
            if (PlayerPrefs.GetInt("Ajudei") == 1)
            {
                Game.Dormir = true;
            }

            else if(PlayerPrefs.GetInt("Ajudei") == 2)
            {
                p_Fade.IN();
            }

            travaFade = true;
        }

        if (SideButton.voltaLevantar == true)
        {

        }

        if (SideButton.voltaVitorCorredor == true)
        {
            StartCoroutine(textVitorCorredor());
        }

        if (SideButton.voltaVitorCorredor2 == true)
        {
            Objetivos.ObjetivoConcluido = true;
            Renders.inicio = true;
            PlayerMovement.Block = true;
            SideButton.voltaVitorCorredor2 = false;
        }
    }

    private IEnumerator textVitorCorredor()
    {
        //Adicionar as frases
        yield return new WaitForSeconds(0.2f);
    }

    private IEnumerator textViewCaixa()
    {
        if (SideButton.voltaCaixa == true)
        {
            travaAudio = true;
            show[0].SetActive(false);
            SideButton.cond = false;
            

            if (SceneManager.GetActiveScene().name == "Madrugada" && !travaEspairecer)
            {
                if (Objetivos.Espairecer < 4)
                {
                    Objetivos.Espairecer += 1;
                    travaEspairecer = true;
                }
            }

            Pensamentos.text = "Espero que não termos que nos mudar de novo. Duas vezes em dois anos, uma terceira não dá não.";
           

            yield return new WaitForSeconds(TemposDeExibicao[2]);
            show[0].SetActive(true);

            Pensamentos.text = "";
            Frases.Interromper = 0;
            travaEspairecer = true;
            SideButton.cond = true;
            travaAudio = false;
            SideButton.voltaCaixa = false;
            
        }
    }


    private IEnumerator textViewPia()
    {
        if (SideButton.voltaPia == true)
        {
            travaAudio = true;
            SideButton.cond = false;

            if (SceneManager.GetActiveScene().name == "Madrugada" && !travaEspairecer)
            {
                if (Objetivos.Espairecer < 4 )
                {
                    Objetivos.Espairecer += 1;
                    travaEspairecer = true;
                }
            }

            Pensamentos.text = "Se algum dia eu tiver uma lava-louças vou ser a pessoa mais feliz do mundo!";
            show[1].SetActive(false);

            yield return new WaitForSeconds(TemposDeExibicao[1]);
            show[1].SetActive(true);

            Pensamentos.text = "";
            Frases.Interromper = 0;
            travaEspairecer = false;
            SideButton.cond = true;
            travaAudio = false;
            SideButton.voltaPia = false;
            
        }
    }

    private IEnumerator textViewJanela()
    {
        if (SideButton.voltaJanela == true)
        {
            travaAudio = true;
            SideButton.cond = false;

            if (SceneManager.GetActiveScene().name == "Madrugada" && !travaEspairecer)
            {
                if (Objetivos.Espairecer < 4)
                {
                    Objetivos.Espairecer += 1;
                    travaEspairecer = true;
                }
            }

            Pensamentos.text = "O Vitor quer muito uma cortina - e eu também. Será que a gente consegue economizar o suficiente?";
            show[2].SetActive(false);

            yield return new WaitForSeconds(TemposDeExibicao[2]);
            show[2].SetActive(true);

            Pensamentos.text = "";
            Frases.Interromper = 0;
            travaEspairecer = false;
            SideButton.cond = true;
            travaAudio = false;
            SideButton.voltaJanela = false;
            
        }
    }

    private IEnumerator camViewCalendario()
    {
        if (SideButton.voltaCalendario == true)
        {
            travaAudio = true;
            SideButton.cond = false;
            camCalendario[0].SetActive(false);
            camCalendario[1].SetActive(true);

            Pensamentos.text = "O aniversário da mamãe está chegando...Vou tentar tirar uma folga do trabalho para sair com o Vitor";
            show[3].SetActive(false);

            yield return new WaitForSeconds(TemposDeExibicao[2]);
            show[3].SetActive(true);

            camCalendario[0].SetActive(true);
            camCalendario[1].SetActive(false);
            Pensamentos.text = "";
            Frases.Interromper = 0;
            SideButton.cond = true;
            travaAudio = false;
            SideButton.voltaCalendario = false;
        }
    }

    private IEnumerator camViewLivros()
    {
        if (SideButton.voltaLivros == true)
        {
            travaAudio = true;
            show[4].SetActive(false);
            SideButton.cond = false;
            camLivros[0].SetActive(false);
            camLivros[1].SetActive(true);
            

            if (SceneManager.GetActiveScene().name == "Madrugada" && !travaEspairecer)
            {
                if (Objetivos.Espairecer < 4)
                {
                    Objetivos.Espairecer += 1;
                    travaEspairecer = true;
                }
            }

            Pensamentos.text = "Será que eu levo um livro para ler no metrô? Não sei se vou ter energia para isso...";
            

            yield return new WaitForSeconds(TemposDeExibicao[2]);
            show[4].SetActive(true);

            camLivros[0].SetActive(true);
            camLivros[1].SetActive(false);
            Pensamentos.text = "";
            Frases.Interromper = 0;
            travaEspairecer = false;
            SideButton.cond = true;
            travaAudio = false;
            SideButton.voltaLivros = false;
        }
    }

    private IEnumerator textViewEspelho()
    {
        if (SideButton.voltaEspelho == true)
        {
            travaAudio = true;
            SideButton.cond = false;
            Pensamentos.text = "Lima...Cê, tá acabado, hein?";
            show[6].SetActive(false);

            yield return new WaitForSeconds(TemposDeExibicao[1]);
            show[6].SetActive(true);

            Pensamentos.text = "";
            Frases.Interromper = 0;
            SideButton.cond = true;
            travaAudio = false;
            SideButton.voltaEspelho = false;
            
        }
    }

    private IEnumerator textViewMural()
    {
        if (SideButton.voltaMural == true)
        {
            travaAudio = true;
            PlayerMovement.Block = true;
            SideButton.cond = false;
            show[7].SetActive(false);            
            Pensamentos.text = "Os vizinhos ainda estão colocando o lixo no lugar errado. Só eu leio esses avisos?";

            yield return new WaitForSeconds(TemposDeExibicao[2]);
            show[7].SetActive(true);
            PlayerMovement.Block = false;
            Pensamentos.text = "";
            Frases.Interromper = 0;
            SideButton.cond = true;
            travaAudio = false;
            SideButton.voltaMural = false;
        }
    }

    private IEnumerator textViewVizinho()
    {
        if (SideButton.voltaVizinho == true)
        {
            travaAudio = true;
            PlayerMovement.Block = true;
            SideButton.cond = false;

            Pensamentos.text = "Eu até queria um cachorro, mas mal consigo cuidar de mim e do meu irmão.";
            show[8].SetActive(false);

            yield return new WaitForSeconds(TemposDeExibicao[2]);
            show[8].SetActive(true);

            Pensamentos.text = "";
            Frases.Interromper = 0;
            PlayerMovement.Block = false;
            SideButton.cond = true;
            travaAudio = false;
            SideButton.voltaVizinho = false;
           
        }
    }

    private IEnumerator textViewContaVizinho()
    {
        if (SideButton.voltaContaVizinho == true)
        {
            travaAudio = true;
            SideButton.cond = false;
            Pensamentos.text = "Puts, eu esqueci! Tenho que fazer as contas dos gastos para o próximo mês. Caramba, não posso esquecer assim...";
            show[9].SetActive(false);

            yield return new WaitForSeconds(TemposDeExibicao[2]);
            show[9].SetActive(true);

            Pensamentos.text = "";
            Frases.Interromper = 0;
            PlayerMovement.Block = false;
            SideButton.cond = true;
            travaAudio = false;
            SideButton.voltaContaVizinho = false;
        }
    }

    private IEnumerator camViewContaLima()
    {
        if (SideButton.voltaContaLima == true)
        {
            travaAudio = true;
            SideButton.cond = false;
            camContaLima[0].SetActive(false);
            camContaLima[1].SetActive(true);

            if (SceneManager.GetActiveScene().name == "Madrugada" && !travaEspairecer)
            {
                if (Objetivos.Espairecer < 4)
                {
                    Objetivos.Espairecer += 1;
                    travaEspairecer = true;
                }
            }

            Pensamentos.text = "Sempre tem alguma coisa para pagar...";
            show[10].SetActive(false);

            yield return new WaitForSeconds(TemposDeExibicao[1]);
            show[10].SetActive(true);

            camContaLima[1].SetActive(false);
            camContaLima[0].SetActive(true);
            Pensamentos.text = "";
            Frases.Interromper = 0;
            travaEspairecer = false;
            SideButton.cond = true;
            travaAudio = false;
            SideButton.voltaContaLima = false;
        }
    }

    private IEnumerator camViewCarpete()
    {
        if (SideButton.voltaCarpete == true)
        {
            PlayerMovement.Block = true;
            player.position = new Vector3(-3.668f, 0.01f, -0.628f);
            player.eulerAngles = new Vector3(0f, 0f, 0f);
            anim.SetBool("Carpete", true);
            animCarpete.SetBool("Arrumar", true);
            SideButton.cond = false;
            SideButton.verificouCarpete = true;
            camCarpete[0].SetActive(false);
            camCarpete[1].SetActive(true);
            show[11].SetActive(false);

            yield return new WaitForSeconds(TemposDeExibicao[1]);
            show[11].SetActive(true);

            camCarpete[1].SetActive(false);
            camCarpete[0].SetActive(true);
            

            anim.SetBool("Carpete", false);
            PlayerMovement.Block = false;
            SideButton.cond = true;
            SideButton.voltaCarpete = false;
            
        }
    }

    private void AbrirPorta()
    {
        InteractionsOFF.TurnOff = true;
        Frases.Parar = true;
        Objetivos.ObjetivoConcluido = true;

        SideButton.cond = false;
        show[12].SetActive(false);
        player.position = new Vector3(-2.38f, 0.01f, -0.25f);
        player.eulerAngles = new Vector3(0f, 0f, 0f);
        anim.SetBool("Abrir Porta", true);
        PensamentosSFX.Stop();
        Frases.Interromper = 1;
        show[12].SetActive(true);
        SideButton.cond = true;
        SideButton.voltaAbrirPorta = false;
    }

    private IEnumerator EstudarGuitarHero()
    {
        backgroundGuitarHero.SetActive(true);
        Frases.Interromper = 1;  
        yield return new WaitForSeconds(1f);
        guitarHero[0].SetActive(true);
        apareceGuitarHero.SetBool("apareceGuitarHero", true);
        //travaEstudo = false;
        PlayerMovement.Block = true;
        SideButton.cond = false;
        show[13].SetActive(false);

        if (spawnButtons.guitarHero == false && spawnButtons.done == true && spawnButtons.podeFecharGH == true && doneGuitarHero.exibiRender == 2)
        {
            travaEstudo = true;
            apareceGuitarHero.SetBool("apareceGuitarHero", false);
            yield return new WaitForSeconds(1f);
            guitarHero[0].SetActive(false);
            backgroundGuitarHero.SetActive(false);
            PlayerMovement.Block = false;
            Game.vaiDormir = true;
            Frases.Interromper = 0;
            SideButton.cond = true;
            SideButton.voltaEstudar = false;
            StopCoroutine(EstudarGuitarHero());
        }
    }

    private IEnumerator textViewVitorJogando()
    {
        if (SideButton.voltaVitorJogando == true)
        {
            travaAudio = true;
            PlayerMovement.Block = true;
            SideButton.cond = false;
            Pensamentos.text = "Saudade de quando eu podia só ficar no sofá jogando com o Vitor. Acho que ele sente falta dos nossos momentos.";
            show[14].SetActive(false);

            yield return new WaitForSeconds(TemposDeExibicao[2]);
            show[14].SetActive(true);

            Pensamentos.text = "";
            Frases.Interromper = 0;
            PlayerMovement.Block = false;
            SideButton.cond = true;
            travaAudio = false;
            SideButton.voltaVitorJogando = false;
            
        }
    }

    private IEnumerator textViewRetratoMae()
    {
        if (SideButton.voltaRetratoMae == true)
        {
            travaAudio = true;
            PlayerMovement.Block = true;
            SideButton.cond = false;
            Pensamentos.text = "Saudadente dos nossos momentos.";
            show[15].SetActive(false);

            yield return new WaitForSeconds(TemposDeExibicao[1]);
            show[15].SetActive(true);

            Pensamentos.text = "";
            Frases.Interromper = 0;
            PlayerMovement.Block = false;
            SideButton.cond = true;
            travaAudio = false;
            SideButton.voltaRetratoMae = false;
            
        }
    }

    private IEnumerator camViewRetratoMae()
    {
        if (SideButton.voltaRetratoMaeAnim == true)
        {
            PlayerMovement.Block = true;
            SideButton.cond = false;
            Pensamentos.text = "";

            show[15].SetActive(false);

            camRetratoMae[0].SetActive(false);
            camRetratoMae[1].SetActive(true);


            yield return new WaitForSeconds(5f);
            show[15].SetActive(true);

            camRetratoMae[0].SetActive(true);
            camRetratoMae[1].SetActive(false);

            Pensamentos.text = "";
            PlayerMovement.Block = false;
            SideButton.voltaRetratoMaeAnim = false;
            SideButton.cond = true;
        }
    }

    private IEnumerator textViewMochila()
    {
        if (SideButton.voltaMochila == true)
        {
            SideButton.cond = false;
            Pensamentos.text = "Essa mochila está meio velha. Mas, já comprei uma nova para o Vitor, então vou ter q esperar...";
            StartCoroutine("waitToSoundClick");
            show[16].SetActive(false);

            yield return new WaitForSeconds(3f);
            show[16].SetActive(true);

            Pensamentos.text = "";
            SideButton.voltaMochila = false;
            SideButton.cond = true;
        }
    }

    private IEnumerator animViewMochila()
    {
        if (SideButton.voltaMochilaAnim == true)
        {

            SideButton.cond = false;
            PlayerMovement.Block = true;
            player.position = new Vector3(1.351779f, 0.00999999f, 1.276f);
            player.eulerAngles = new Vector3(0f, 4.578f, 0f);
            animPlayer.SetBool("Mochila", true);
            show[16].SetActive(false);

            PlayerMovement.Block = false;
            yield return new WaitForSeconds(2f);



            //animPlayer.SetBool("Mochila", false);
            Mochilinha[0].SetActive(false);
            Mochilinha[1].SetActive(true);
            Objetivos.Mochila = 1;
            SideButton.voltaMochilaAnim = false;
            SideButton.cond = true;
        }
    }

    private IEnumerator textViewCarteira()
    {
        if (SideButton.voltaCarteira == true)
        {
            SideButton.cond = false;
            Pensamentos.text = "Eu sempre me sento aqui. Será que eu deveria mudar de carteira?";
            StartCoroutine("waitToSoundClick");
            show[17].SetActive(false);

            yield return new WaitForSeconds(3f);
            show[17].SetActive(true);

            Pensamentos.text = "";
            SideButton.voltaCarteira = false;
            SideButton.cond = true;
        }
    }

    private IEnumerator textViewCamAmigos()
    {
        if (SideButton.voltaAmigos == true)
        {
            SideButton.cond = false;
            PlayerMovement.Block = true;
            Pensamentos.text = "Júlia e Lucas são praticamente as únicas pessoas que converso na faculdade - eles são legais, queria ter mais tempo para me enturmar";
            show[18].SetActive(false);


            camAmigos[0].SetActive(false);
            camAmigos[1].SetActive(true);

            yield return new WaitForSeconds(4.5f);
            show[18].SetActive(true);

            PlayerMovement.Block = false;
            camAmigos[0].SetActive(true);
            camAmigos[1].SetActive(false);

            Pensamentos.text = "";
            SideButton.voltaAmigos = false;
            SideButton.cond = true;
        }
    }

    private IEnumerator CamViewMarmita()
    {
        if (SideButton.voltaMarmita == true)
        {

            if (trava == false)
            {
                SideButton.cond = false;
                PlayerMovement.Block = true;
                camMarmita[0].SetActive(false);
                camMarmita[1].SetActive(true);

                portaGeladeira[0].SetActive(false);
                portaGeladeira[1].SetActive(true);

                player.position = new Vector3(-1.884f, 0.01f, 1.507f);
                player.eulerAngles = new Vector3(0f, -89.507f, 0f);

                animPlayer.SetBool("setMarmita", true);
                show[19].SetActive(false);

                yield return new WaitForSeconds(5f);

                if (trava2 == false)
                {
                    fade.SetBool("In", true);
                    trava2 = true;
                }
                yield return new WaitForSeconds(1f);

                fade.SetBool("In", false);

                portaGeladeira[0].SetActive(true);
                portaGeladeira[1].SetActive(false);
                marmita.SetActive(true);

                camMarmita[1].SetActive(false);
                camMarmita[2].SetActive(true);


                player.position = new Vector3(-2.178f, -0.03809042f, 2.935f);
                player.eulerAngles = new Vector3(0f, -90.368f, 0f);
                trava = true;
            }

            camMarmita[2].SetActive(false);
            camMarmita[0].SetActive(true);

            yield return new WaitForSeconds(12f);
            animPlayer.SetBool("setMarmita", false);
            PlayerMovement.Block = false;
            Pensamentos.text = "";
            show[20].SetActive(true);
            show[20].GetComponent<SpriteRenderer>().enabled = true;
            Objetivos.PrepararMarmita = 1;
            trava3 = true;
            Objetivos.CoisasDeDormir = true;
            SideButton.cond = true;
            SideButton.voltaMarmita = false;
            

        }
    }

    IEnumerator waitToSoundClick()
    {
        yield return new WaitForSeconds(2f);
        StopCoroutine("waitToSoundClick");
    }

}

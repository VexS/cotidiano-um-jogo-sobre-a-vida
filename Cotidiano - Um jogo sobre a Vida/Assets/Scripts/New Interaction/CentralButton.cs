﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CentralButton : MonoBehaviour
{
    public GameObject[] show;
    public SpriteRenderer[] _SpritesPorta;

    private bool alreadyUP = false;
    private bool Interact;
    private int cont = 0;

    public AudioClip somClick;

    private AudioSource audioSource;

    private SpriteRenderer render;
    Color cor;

    private Transform diminuirSprite;

    private Animator m_Anim;
    public static bool Volta;

    private void Awake()
    {
        Volta = false;
        Interact = false;
    }

    void Start()
    {
        m_Anim = this.GetComponent<Animator>();
        render = this.gameObject.GetComponent<SpriteRenderer>();

        if (this.name != "Carpete")
        {
            cor = this.render.color;
            cor.a = .4f;
            this.render.color = cor;
        }

        show[0].SetActive(false);
        show[1].SetActive(false);

        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        if (this.Interact == false && this.gameObject.name != "Carpete" && this.gameObject.name != "VitorCorredor")
        {
            cor.a = .4f;
            this.render.color = new Color(1f, 1f, 1f, .4f);
        }
        else if (this.Interact == true)
        {
            cor.a = .65f;
            this.render.color = cor;
        }

        if (Volta)
        {
            m_Anim.SetBool("Voltar", true);
            Volta = false;
        }

        if (SideButton.voltaAbrirPorta)
        {
            foreach (SpriteRenderer item in _SpritesPorta)
            {
                item.enabled = false;
            }
        }

        if (cont >= 2)
        {
            cont = 0;
        }
    }

    public void CanInteract()
    {
        Interact = true;
    }

    public void CannotInteract()
    {
        Interact = false;
    }

    public void OnMouseOver()
    {
        if (this.m_Anim != null && !alreadyUP && Interact)
        {
            m_Anim.SetBool("Expandir", true);
            m_Anim.SetBool("Voltar", false);
        }
        else if (this.m_Anim != null && this.name == "Carpete" || this.name == "Cama" || this.name == "Vitor" && !alreadyUP)
        {
            m_Anim.SetBool("Expandir", true);
            m_Anim.SetBool("Voltar", false);
        }
    }

    public void OnMouseExit()
    {
        if (this.m_Anim != null && !alreadyUP && Interact)
        {
            m_Anim.SetBool("Expandir", false);
            m_Anim.SetBool("Voltar", true);
        }
        else if (this.m_Anim != null && this.name == "Carpete" || this.name == "Cama" || this.name == "Vitor" && !alreadyUP)
        {
            m_Anim.SetBool("Expandir", false);
            m_Anim.SetBool("Voltar", true);
        }
    }

    public void OnMouseDown()
    {
        if (Interact && this.name != "Carpete" && this.name != "Vitor")
        {
            if (alreadyUP == false && cont == 0)
            {
                audioSource.PlayOneShot(somClick);

                if (this.m_Anim != null)
                {
                    m_Anim.SetBool("Expandir", false);
                    m_Anim.SetBool("Reduzir", true);
                }

                if (this.gameObject.layer == 8)
                {
                    show[0].SetActive(true);
                }
                else if (this.gameObject.layer == 10)
                {
                    show[1].SetActive(true);
                }
                else
                {
                    show[0].SetActive(true);
                    show[1].SetActive(true);
                }

                alreadyUP = true;
                cont = 1;
            }

            else if (alreadyUP == true && cont == 1)
            {
                audioSource.PlayOneShot(somClick);
                show[0].SetActive(false);
                show[1].SetActive(false);

                if (this.m_Anim != null)
                {
                    m_Anim.SetBool("Reduzir", false);
                }

                alreadyUP = false;
                cont = 2;
            }
        }

        if (this.name == "Carpete")
        {
            if (alreadyUP == false && cont == 0)
            {
                audioSource.PlayOneShot(somClick);

                if (this.m_Anim != null)
                {
                    m_Anim.SetBool("Expandir", false);
                    m_Anim.SetBool("Reduzir", true);
                }

                if (!Interact)
                {
                    show[0].SetActive(true);
                }
                else if (Interact)
                {
                    show[0].SetActive(true);
                    show[1].SetActive(true);
                }

                alreadyUP = true;
                cont = 1;
            }

            else if (alreadyUP == true && cont == 1)
            {
                audioSource.PlayOneShot(somClick);

                if (!Interact)
                {
                    show[0].SetActive(false);
                }

                else if (Interact)
                {
                    show[0].SetActive(false);
                    show[1].SetActive(false);
                }

                if (this.m_Anim != null)
                {
                    m_Anim.SetBool("Reduzir", false);
                }

                alreadyUP = false;
                cont = 2;
            }
        }

        if (this.name == "Vitor")
        {
            if (alreadyUP == false && cont == 0)
            {
                audioSource.PlayOneShot(somClick);

                if (this.m_Anim != null)
                {
                    m_Anim.SetBool("Expandir", false);
                    m_Anim.SetBool("Reduzir", true);
                }

                if (!Interact)
                {
                    show[0].SetActive(true);
                }

                else if (Interact)
                {
                    show[0].SetActive(true);
                    show[1].SetActive(true);
                }

                alreadyUP = true;
                cont = 1;
            }

            else if (alreadyUP == true && cont == 1)
            {
                audioSource.PlayOneShot(somClick);

                if (!Interact)
                {
                    show[0].SetActive(false);
                }

                else if (Interact)
                {
                    show[0].SetActive(false);
                    show[1].SetActive(false);
                }

                if (this.m_Anim != null)
                {
                    m_Anim.SetBool("Reduzir", false);
                }

                alreadyUP = false;
                cont = 2;
            }
        }

        if (this.name == "Dormir")
        {
            if (alreadyUP == false && cont == 0)
            {
                audioSource.PlayOneShot(somClick);

                if (this.m_Anim != null)
                {
                    m_Anim.SetBool("Expandir", false);
                    m_Anim.SetBool("Reduzir", true);
                }

                alreadyUP = true;
                cont = 1;
            }

            else if (alreadyUP == true && cont == 1)
            {
                audioSource.PlayOneShot(somClick);

                if (this.m_Anim != null)
                {
                    m_Anim.SetBool("Reduzir", false);
                }

                alreadyUP = false;
                cont = 2;
            }
        }

        if (this.name == "Cama")
        {
            if (alreadyUP == false && cont == 0)
            {
                audioSource.PlayOneShot(somClick);

                if (this.m_Anim != null)
                {
                    m_Anim.SetBool("Expandir", false);
                    m_Anim.SetBool("Reduzir", true);
                }

                alreadyUP = true;
                cont = 1;
            }

            else if (alreadyUP == true && cont == 1)
            {
                audioSource.PlayOneShot(somClick);

                if (this.m_Anim != null)
                {
                    m_Anim.SetBool("Reduzir", false);
                }

                alreadyUP = false;
                cont = 2;
            }
        }
    }
}


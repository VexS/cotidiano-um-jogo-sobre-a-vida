﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SideButton : MonoBehaviour
{
    public GameObject[] show;

    public TextMeshProUGUI Pensamentos;

    public static bool voltaCaixa;
    public static bool voltaPia;
    public static bool voltaJanela;
    public static bool voltaCalendario;
    public static bool voltaLivros;
    public static bool voltaEspelho;
    public static bool voltaMural;
    public static bool voltaVizinho;
    public static bool voltaContaVizinho;
    public static bool voltaContaLima;
    public static bool voltaCarpete;
    public static bool voltaAbrirPorta;
    public static bool voltaEstudar;
    public static bool voltaVitorJogando;
    public static bool voltaRetratoMae;
    public static bool voltaRetratoMaeAnim;
    public static bool voltaMochila;
    public static bool voltaMochilaAnim;
    public static bool voltaCarteira;
    public static bool voltaAmigos;
    public static bool voltaAmigos2;
    public static bool voltaDormir;
    public static bool voltaLevantar;
    public static bool voltaMarmita;
    public static bool voltaCama3;

    public static bool voltaVitorCorredor;

    public static bool voltaVitorCorredor2;

    public static bool cond = true;

    public static bool verificouCarpete;

    public AudioClip somClick;

    private AudioSource audioSource;

    private void Awake()
    {
        voltaCaixa = false;
        voltaPia = false;
        voltaJanela = false;
        voltaCalendario = false;
        voltaLivros = false;
        voltaEspelho = false;
        voltaMural = false;
        voltaVizinho = false;
        voltaContaVizinho = false;
        voltaContaLima = false;
        voltaCarpete = false;
        voltaAbrirPorta = false;
        voltaEstudar = false;
        voltaVitorJogando = false;
        voltaRetratoMae = false;
        voltaRetratoMaeAnim = false;
        voltaMochila = false;
        voltaMochilaAnim = false;
        voltaCarteira = false;
        voltaAmigos = false;
        voltaAmigos2 = false;
        voltaDormir = false;
        voltaLevantar = false;
        voltaMarmita = false;
        voltaCama3 = false;

        voltaVitorCorredor = false;

        voltaVitorCorredor2 = false;

        verificouCarpete = false;
}

    private void Start()
    {
        audioSource = this.GetComponent<AudioSource>();
    }

    private void Update()
    {
       
    }

    private void OnMouseUp()
    {
        if (cond == true)
        {
            if (gameObject.CompareTag("sideButtonCaixa"))
            {
                CentralButton.Volta = true;
                voltaCaixa = true;
                show[0].SetActive(false);
                show[1].SetActive(false);

            }

            if (gameObject.CompareTag("sideButtonPia"))
            {
                CentralButton.Volta = true;
                voltaPia = true;
                show[0].SetActive(false);
                show[1].SetActive(false);
            }

            if (gameObject.CompareTag("sideButtonJanela"))
            {
                CentralButton.Volta = true;
                voltaJanela = true;
                show[0].SetActive(false);
                show[1].SetActive(false);
            }

            if (gameObject.CompareTag("sideButtonCalendario2"))
            {
                CentralButton.Volta = true;
                voltaCalendario = true;
                show[0].SetActive(false);
                show[1].SetActive(false);
            }


            if (gameObject.CompareTag("sideButtonLivros"))
            {
                CentralButton.Volta = true;
                voltaLivros = true;
                show[0].SetActive(false);
                show[1].SetActive(false);
            }

            if (gameObject.CompareTag("sideButtonEspelho"))
            {
                CentralButton.Volta = true;
                voltaEspelho = true;
                show[0].SetActive(false);
                show[1].SetActive(false);
            }

            if (gameObject.CompareTag("sideButtonMural"))
            {
                CentralButton.Volta = true;
                voltaMural = true;
                show[0].SetActive(false);
                show[1].SetActive(false);
            }

            if (gameObject.CompareTag("sideButtonVizinho"))
            {
                CentralButton.Volta = true;
                voltaVizinho = true;
                show[0].SetActive(false);
                show[1].SetActive(false);
            }

            if (gameObject.CompareTag("sideButtonContaVizinho"))
            {
                CentralButton.Volta = true;
                voltaContaVizinho = true;
                show[0].SetActive(false);
                show[1].SetActive(false);
            }

            if (gameObject.CompareTag("sideButtonContaLima"))
            {
                CentralButton.Volta = true;
                voltaContaLima = true;
                show[0].SetActive(false);
                show[1].SetActive(false);
            }

            if (gameObject.CompareTag("sideButtonCarpete") && verificouCarpete == false && voltaCarpete == false)
            {
                StartCoroutine("OlharCarpeteLonge");
                CentralButton.Volta = true;
                show[0].SetActive(false);
                show[1].SetActive(false);
            }

            else if (gameObject.CompareTag("sideButtonCarpete2") && Collisions.podeAnim == false && verificouCarpete == false)
            {
                StartCoroutine("CarpeteDesarrumado");
                CentralButton.Volta = true;
            }

            else if (gameObject.CompareTag("sideButtonCarpete2") && Collisions.podeAnim == true && verificouCarpete == false)
            {
                CentralButton.Volta = true;
                voltaCarpete = true;
                show[0].SetActive(false);
                show[1].SetActive(false);

            }

            if (gameObject.CompareTag("sideButtonCarpete2") && verificouCarpete == true)
            {
                CentralButton.Volta = true;
                StartCoroutine("CarpeteArrumado");
            }

            if (gameObject.CompareTag("sideButtonAbrirPorta2"))
            {
                CentralButton.Volta = true;
                voltaAbrirPorta = true;
                show[0].SetActive(false);
                show[1].SetActive(false);
            }

            if (gameObject.CompareTag("sideButtonEstudar"))
            {
                CentralButton.Volta = true;
                show[0].SetActive(false);
                show[1].SetActive(false);
            }

            else if (gameObject.CompareTag("sideButtonEstudar2"))
            {
                CentralButton.Volta = true;
                voltaEstudar = true;
                show[0].SetActive(false);
                show[1].SetActive(false);
            }

            if (gameObject.CompareTag("sideButtonVitorJogando"))
            {
                CentralButton.Volta = true;
                voltaVitorJogando = true;
                show[0].SetActive(false);
                show[1].SetActive(false);
            }

            if (gameObject.CompareTag("sideButtonRetratoMae"))
            {

                CentralButton.Volta = true;
                voltaRetratoMae = true;
                show[0].SetActive(false);
                show[1].SetActive(false);
            }

            if (gameObject.CompareTag("sideButtonRetratoMae2"))
            {
                CentralButton.Volta = true;
                voltaRetratoMaeAnim = true;
                show[0].SetActive(false);
                show[1].SetActive(false);
            }

            if (gameObject.CompareTag("sideButtonMochila"))
            {
                voltaMochila = true;
                DontDestroyOnLoad(audioSource);
                tocaSomSideButton();
                show[0].SetActive(false);
                show[1].SetActive(false);
            }

            if (gameObject.CompareTag("sideButtonMochila2"))
            {
                voltaMochilaAnim = true;
                show[0].SetActive(false);
                show[1].SetActive(false);
            }

            if (gameObject.CompareTag("sideButtonCarteira"))
            {
                voltaCarteira = true;
                DontDestroyOnLoad(audioSource);
                tocaSomSideButton();
                show[0].SetActive(false);
                show[1].SetActive(false);
            }

            if (gameObject.CompareTag("sideButtonCarteira2"))
            {
                voltaCarteira = true;
                show[0].SetActive(false);
                show[1].SetActive(false);
            }

            if (gameObject.CompareTag("sideButtonAmigos"))
            {
                voltaAmigos = true;
                DontDestroyOnLoad(audioSource);
                tocaSomSideButton();
                show[0].SetActive(false);
                show[1].SetActive(false);
            }

            if (gameObject.CompareTag("sideButtonAmigos2"))
            {
                voltaAmigos2 = true;
                show[0].SetActive(false);
                show[1].SetActive(false);
            }

            if (gameObject.CompareTag("sideButtonCama2"))
            {
                voltaDormir = true;
                show[1].SetActive(false);
            }

            if (gameObject.CompareTag("sideButtonVitorCorredor"))
            {
                voltaVitorCorredor = true;
                DontDestroyOnLoad(audioSource);
                tocaSomSideButton();
                show[0].SetActive(false);
                show[1].SetActive(false);
            }

            if (gameObject.CompareTag("sideButtonVitorCorredor2"))
            {
                voltaVitorCorredor2 = true;
                show[0].SetActive(false);
                show[1].SetActive(false);
            }

            if (gameObject.CompareTag("sideButtonMarmita"))
            {

                show[0].SetActive(false);
                show[1].SetActive(false);
            }

            if (gameObject.CompareTag("sideButtonMarmita2") && Objetivos.Espairecer >= 5)
            {
                voltaMarmita = true;
                show[0].SetActive(false);
                show[1].SetActive(false);
            }

            if (gameObject.CompareTag("sideButtonCamaDois2") && InteractionController.trava3)
            {
                voltaCama3 = true;
                show[0].SetActive(false);
                show[1].SetActive(false);
            }
        }
    }

    private void tocaSomSideButton()
    {
        audioSource.PlayOneShot(somClick);
    }

    IEnumerator CarpeteDesarrumado()
    {
        Pensamentos.text = "Se eu chegar em casa e me arrumar em meia hora, vou ter uma hora para estudar e dormir...";

        yield return new WaitForSeconds(3f);

        Pensamentos.text = "";
    }

    IEnumerator CarpeteArrumado()
    {
        Pensamentos.text = "Está bem melhor agora.";

        yield return new WaitForSeconds(3f);

        Pensamentos.text = "";
    }

    IEnumerator OlharCarpeteLonge()
    {
        Pensamentos.text = "Isso está me incomodando, talvez se eu arrumar não de problemas";

        yield return new WaitForSeconds(1.9f);

        Pensamentos.text = "";
        StopCoroutine("OlharCarpeteLonge");
    }
}


﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Cinemachine;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;

public class Game : MonoBehaviour
{

    public GameObject[] Escolhas;

    public GameObject Player;

    public GameObject ProvaMinigame;

    public GameObject[] InteracoesTimeline;

    public Animator _animator;
    
    public PlayableAsset[] Asset;

    public PlayableDirector playableDirector;


    public static bool Teste;

    public static bool cenaLevantar;

    public static bool cenaDeitar;

    private bool[] Prova = new bool[2];

    public static bool avisoMochila;

    public static bool posVitor;

    public static bool posElevador;

    public static bool FrasesElevador;

    public static bool FrasesInicio;

    public static bool vendoVitor;

    public static bool FrasesVitor;

    public static int _Tutorial;

    public static bool brainChange;

    public static bool Dormir;

    public static bool vaiDormir;

    public static bool Estudei;

    public static bool Conversa;

    public GameObject[] InteracaoDormir;


    public TextMeshProUGUI _Aviso;

    public TextMeshProUGUI Pensamentos;

    public CinemachineBrain _Brain;

    public AudioSource audio_Pensamento;

    public AudioClip[] pensamentos_Interacoes;

    public AudioClip[] pensamentos_Falas;

    private bool MochilaAudio;

    public SpriteRenderer[] Interacoes;

    public GameObject[] InteracoesDESLIGAR;

    private void Awake()
    {
        if(SceneManager.GetActiveScene().name == "Noite")
        {
            posElevador = true;
            Player.GetComponent<Animator>().SetBool("m_Andar", true);
        }

        Conversa = false;
        Dormir = false;
        vaiDormir = false;
        Estudei = false;
        Teste = false;
        cenaDeitar = false;
        cenaLevantar = false;
        MochilaAudio = false;
        Prova[0] = false;
        Prova[1] = false;
        avisoMochila = false;
        vendoVitor = false;
        posVitor = false;
        FrasesElevador = false;
        FrasesInicio = false;
        FrasesVitor = false;
        _Tutorial = -1;
    }

    // Start is called before the first frame update
    void Start()
    {
        brainChange = false;

        if (SceneManager.GetActiveScene().name == "Corredor")
        {
            PlayerMovement.Block = true;
            Player.GetComponent<Animator>().SetBool("m_Andar", true);
        } 
    }

    // Update is called once per frame
    void Update()
    {
        if(Conversa)
        {
            InteracoesDESLIGAR[0].SetActive(false);
            InteracoesDESLIGAR[1].SetActive(false);
            InteracoesDESLIGAR[2].SetActive(false);
        }

        if(vaiDormir)
        {
            InteracaoDormir[0].GetComponent<SpriteRenderer>().enabled = true;
            InteracaoDormir[1].SetActive(true);
            InteracaoDormir[0].SetActive(true);
            vaiDormir = false;
        }

        if (Dormir)
        {
            Player.GetComponent<Rigidbody>().isKinematic = true;
            Player.GetComponent<CapsuleCollider>().enabled = false;
            Player.GetComponent<Animator>().applyRootMotion = false;
            InteracaoDormir[0].SetActive(false);
            playableDirector.playableAsset = Asset[2];
            playableDirector.Play();
            Dormir = false;
        }

        if(Estudei)
        {
            foreach(SpriteRenderer item in Interacoes)
            {
                item.enabled = true;
            }

            Estudei = false;
        }

        if(brainChange)
        {
            _Brain.m_DefaultBlend.m_Style = CinemachineBlendDefinition.Style.EaseInOut;
        }

        else if(!brainChange)
        {
            _Brain.m_DefaultBlend.m_Style = CinemachineBlendDefinition.Style.Cut;
        }

        if (_Tutorial == 0)
        {
            _Aviso.text = "Aperte O para checar seus objetivos.";
            PlayerMovement.Block = true;
        }

        if (_Tutorial == 1)
        {
            _Aviso.text = "Use as teclas W, A, S e D para se mover";
            PlayerMovement.Block = false;
        }
        else if(_Tutorial == 2)
        {
            _Aviso.text = "";
            Frases.frases = 1;
            _Tutorial = -1;
        }

        if(SceneManager.GetActiveScene().name == "Apartamento" && cenaLevantar) // Aciona a cena do personagem levantando da cama
        {
            playableDirector.playableAsset = Asset[0];
            playableDirector.Play();
            cenaLevantar = false;
        }

        if(SceneManager.GetActiveScene().name == "Faculdade" && Teste) // Liga o teste da Prova na Faculdaded
        {
            //ProvaMinigame.SetActive(true);
            //ProvaMinigame.GetComponent<Animator>().SetBool("apareceGuitarHero", true);
            canPressGH.pontos = 6;
            LigarPlayer();
            Teste = false;
        }

        /*if(Input.GetKeyDown(KeyCode.M))
        {
            Prova[0] = true;
            Prova[1] = false;
            StartCoroutine(PSP());
        }
        else if(Input.GetKeyDown(KeyCode.N))
        {
            Prova[0] = false;
            Prova[1] = true;
            StartCoroutine(PSP());
        }

        if(Input.GetKeyDown(KeyCode.V))
        {
            posVitor = true;
        }*/

        if(avisoMochila)
        {
            MochilaAudio = true;
            StartCoroutine(PSP());
            avisoMochila = false;
        }

        if(posVitor)
        {
            StartCoroutine(PSP());
            posVitor = false;
        }

        else if(posElevador)
        {
            FrasesElevador = true;
            StartCoroutine(PSP());
            posElevador = false;
        }

        if(vendoVitor)
        {
            if(Pensamentos.text == "")
            {
                FrasesVitor = true;
                StartCoroutine(PSP());
                vendoVitor = false;
            }
        }
    }

    IEnumerator PSP()
    {
        if(Prova[0])
        {
            yield return new WaitForSeconds(3f);
            Pensamentos.text = "Que  alívio!  Até  que  estava  tranquilo, espero   que   o   Lucas   tenha   ido   bem também...";
        }

        else if(Prova[1])
        {
            yield return new WaitForSeconds(3f);
            Pensamentos.text = "Cara...vou ter que estudar muito para compensar  essa  prova. Deu tudo errado!";
        }

        if(MochilaAudio)
        {
            Pensamentos.text = "Puts, a mochila";
            //audio_Pensamento.Stop();
            //audio_Pensamento.PlayOneShot(pensamentos_Interacoes[0]);
            MochilaAudio = false;
        }

        if(posVitor)
        {
            Pensamentos.text = "Que estranho, ele nunca me pergunta isso... Será se chegou naquela fase? Ô, Deus....";
            posVitor = false;
        }

        if(FrasesElevador)
        {
            Pensamentos.text = "Não sei se foi uma boa escolha desistir da hora extra de amanhã.... mas, eu acho que mereço um pouco de descanso, vai?";
            yield return new WaitForSeconds(3.75f);
            Pensamentos.text = "Além do mais, a Júlia é bem legal! Vai ser divertido; Relaxa, Lima.";
            yield return new WaitForSeconds(2.75f);
            Pensamentos.text = "";
            StopCoroutine(PSP()); 
            FrasesElevador = false;    
        }

        if(FrasesVitor)
        {
            Pensamentos.text = "Vitor? O que ele está fazendo do lado de fora de casa?";
            yield return new WaitForSeconds(2.75f);
            Pensamentos.text = "Será que aconteceu alguma coisa?";
            yield return new WaitForSeconds(2.75f);
            Pensamentos.text = "";
            FrasesVitor = false;
            StopCoroutine(PSP());   
        }

        yield return new WaitForSeconds(5.5f);
        Pensamentos.text = "";
        StopCoroutine(PSP());
    }

    public void Restaurar()
    {
        Player.transform.position = new Vector3(2.181f, 0.01f, 0.948f);

        _animator.applyRootMotion = true;
        _animator.gameObject.GetComponent<CapsuleCollider>().enabled = true;
        _animator.gameObject.GetComponent<Rigidbody>().isKinematic = false;

        if (SceneManager.GetActiveScene().name == "Apartamento")
        {
            Estudei = true;
        }
    }

    public void LigarPlayer()
    {
        Player.SetActive(true);
        Objetivos.mostrar2 = 0;
        Objetivos.bloqueio = false;
        
    }
}

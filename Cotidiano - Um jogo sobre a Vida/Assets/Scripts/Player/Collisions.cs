﻿using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collisions : MonoBehaviour
{
    public GameObject[] Cams;
    private GameObject[] Interactions;
    static string camName;
    string interactionName;

    Animator anim;
    public Animator animPorta;

    public static bool podeAnim;

    private void Awake()
    {
        podeAnim = false;
    }

    private void Start()
    {
        anim = GetComponent<Animator>();
        Cams = GameObject.FindGameObjectsWithTag("CV");
        Interactions = GameObject.FindGameObjectsWithTag("IC");
    }

    private void Update()
    {
        if (Game.brainChange == true)
        {
            foreach (GameObject cam in Cams)
            {
                cam.GetComponent<CinemachineVirtualCamera>().Priority = 0;              
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.name == "VerVitor")
        {
            Game.vendoVitor = true;
            other.gameObject.SetActive(false);
        }

        // Ativação das Câmeras Virtuais por Trigger;
        if(other.CompareTag("CMBC"))
        {
            PlayerMovement.Wait = true;
            camName = other.gameObject.name;

            foreach(GameObject cam in Cams)
            {
                if (cam.name == camName)
                {
                    cam.GetComponent<CinemachineVirtualCamera>().Priority = 1;
                }
                else
                cam.GetComponent<CinemachineVirtualCamera>().Priority = 0;
            }
        }

        // Ativação das bools para ativar as interações;
        if (other.CompareTag("ICBC"))
        {
            interactionName = other.gameObject.name;

            for (int i = 0; i < Interactions.Length; i++)
            {
                if (Interactions[i].name == interactionName)
                {
                    Interactions[i].GetComponent<CentralButton>().CanInteract();
                }
            }
        }

        if (other.CompareTag("Carpete"))
        { 
            podeAnim = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Carpete"))
        {
            podeAnim = false;
        }

        if (other.CompareTag("ICBC"))
        {
            interactionName = other.gameObject.name;

            for (int i = 0; i < Interactions.Length; i++)
            {
                if (Interactions[i].name == interactionName)
                {
                    Interactions[i].GetComponent<CentralButton>().CannotInteract();
                }
            }
        }
    }

    public void Olhar()
    {
        anim.SetBool("Olhar", true);
        anim.SetBool("Abrir Porta", false);
        this.GetComponent<Rigidbody>().isKinematic = true;
    }

    public void Escolher()
    {
        //ligar botoes
        anim.SetBool("Olhar", false);
        Choices.ChooseYourFate = true;
    }

    public void Porta()
    {
        //
        animPorta.SetBool("Abrir", true);
    }
}

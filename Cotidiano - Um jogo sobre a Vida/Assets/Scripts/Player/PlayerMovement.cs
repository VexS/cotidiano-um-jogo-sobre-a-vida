﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float velocity = 5f;    
    public float RotateSpeed = 5f;

    public static bool Moving;
    public static bool Block;
    public static bool Wait = false;

    Vector2 input;
    Vector2 inputMov;
    float angle;
	float camAngle;

	Quaternion targetRotation;
    Transform cam;

    CharacterController controller;
    Rigidbody body;
    Animator anim;

    private void Awake()
    {
        Block = false;
		Wait = false;
        Moving = false;
    }

    private void Start()
    {
        cam = Camera.main.transform;
        controller = GetComponent<CharacterController > ();
        body = GetComponent<Rigidbody > ();
        anim = GetComponent<Animator> ();
    }

    private void Update()
    {
		if (inputMov == Vector2.zero)
		{
			Wait = false;
		}

		if (Block)
        {
            anim.SetBool("Walking", false);
        }

        else if (!Block)
        {
            // Animação
            inputMov.x = Input.GetAxis("Horizontal");
            inputMov.y = Input.GetAxis("Vertical");
            inputMov = Vector2.ClampMagnitude(inputMov, 1);

            if (inputMov != Vector2.zero) Moving = true;
            else if (inputMov == Vector2.zero) Moving = false;

            anim.SetBool("Walking", Moving);
           
            //

            // Movimentação
            GetInput();

            if (Mathf.Abs(input.x) < 1 && Mathf.Abs(input.y) < 1)
            {
                return;
	        }

            CalculateDirection();
            Rotate();
            Move();
        }
        //
    } 

    void GetInput()
    {
        input.x = Input.GetAxisRaw("Horizontal");
        input.y = Input.GetAxisRaw("Vertical");
    }

    void CalculateDirection()
    {
        angle = Mathf.Atan2(input.x, input.y);
        angle = Mathf.Rad2Deg * angle;

		if (!Wait)
		{
			camAngle = cam.eulerAngles.y;
			angle += camAngle;
		}

		else if (Wait && camAngle != cam.eulerAngles.y)
		{
			angle += camAngle;
		}
	}

    void Rotate()
    {
        targetRotation = Quaternion.Euler(0, angle, 0);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, RotateSpeed * Time.deltaTime);
    }

    void Move()
    {
        if (Game._Tutorial == 1)
        {
            Game._Tutorial = 2;
        }

        body.AddForce(transform.forward * Time.deltaTime * velocity, ForceMode.VelocityChange);
    }
}

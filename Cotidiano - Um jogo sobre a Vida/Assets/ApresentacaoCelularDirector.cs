﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class ApresentacaoCelularDirector : MonoBehaviour
{
    public PlayableDirector Vexinho;
    public static bool Apresentacao;

    // Update is called once per frame
    void Update()
    {
        Vexinho.Stop();

        Vexinho.extrapolationMode = DirectorWrapMode.None;
    }
}

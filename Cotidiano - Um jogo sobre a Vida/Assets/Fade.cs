﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Fade : MonoBehaviour
{
    public Renders Renders;
    public Image ultimoFrame; 

    private Animator m_Anim;

    // Start is called before the first frame update
    void Start()
    {
        m_Anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void IN()
    {
        m_Anim.SetBool("In", true);
        m_Anim.SetBool("Out", false);
    }

    public void Out()
    {
        m_Anim.SetBool("Out", true);
        m_Anim.SetBool("In", false);
    }

    public void DesligarFrame()
    {
        if (SceneManager.GetActiveScene().name == "Faculdade")
        {
            RendersFaculdade.manter = false;
            ultimoFrame.enabled = false;
        }
    }

    public void Faculdade()
    {
        m_Anim.SetBool("Out", false);

        if (SceneManager.GetActiveScene().name == "Faculdade")
        {
            Game.Teste = true;
        }
    }

    public void OutEnd()
    {
        if (SideButton.voltaDormir)
        {
            Renders.Consequencia();
            Out();
            SideButton.voltaDormir = false;
        }
        else Out();
    }
}

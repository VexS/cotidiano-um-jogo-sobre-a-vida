﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HorarioCelular : MonoBehaviour
{
    public GameObject[] HorariosCelular;
    //HORARIO 0 = 23H11
    //HORARIO 1 = 23H11 -> horário na conversa
    //HORARIO 2 = 01h00 -> horário na conversa
    //HORARIO 3 = 01h00 -> horário no whats
    //HORARIO 4 = 01h00 -> horário que recebeu a ultima mensagem

    public GameObject[] MiguePraTrocar;
    //MIGUE PRA TAPAR O HORÁRIO DA CONVERSA 0
    //MIGUE PRA TAPAR A ULTIMA FRASE DA CONVERSA; 1
    //ULTIMA FALA DA CONVERSA 2 || SE ELE ESCOLHEU AJUDAR
    //ULTIMA FALA DA CONVERSA 3 || SE ELE ESCOLHEU ESTUDAR

    public GameObject[] HorarioOutrasConversas;
    //HORARIO 0 = 23H11 -> VITOR
    //HORARIO 1 = 01H00 -> VITOR
    //HORARIO 2 = 23H11 -> MARCELO
    //HORARIO 3 = 01H00 -> MARCELO


    void Update()
    {
        if (Celular.Ajudou == true)
        {
            MiguePraTrocar[2].SetActive(true);  
        }

        if (Celular.passouTimeSkip == true) 
        {
            MiguePraTrocar[0].SetActive(true);
            MiguePraTrocar[1].SetActive(true);
            MiguePraTrocar[2].SetActive(true);
            HorariosCelular[0].SetActive(false);
            HorariosCelular[1].SetActive(false);
            HorariosCelular[2].SetActive(true);
            HorariosCelular[3].SetActive(true);
            HorariosCelular[4].SetActive(true);

        }

        if (Celular.Estudou == true)
        {
            MiguePraTrocar[0].SetActive(true);
            MiguePraTrocar[1].SetActive(true);
            MiguePraTrocar[3].SetActive(true);
        }

        if (HorariosCelular[0].activeInHierarchy) //SE TIVER 23H11 NAS OUTRAS CONVERSAS
        {
            HorarioOutrasConversas[0].SetActive(true);
            HorarioOutrasConversas[2].SetActive(true);
        }
        
        if (HorariosCelular[2].activeInHierarchy)
        {
            HorarioOutrasConversas[0].SetActive(false);
            HorarioOutrasConversas[2].SetActive(false);
            HorarioOutrasConversas[1].SetActive(true);
            HorarioOutrasConversas[3].SetActive(true);
        }
    }
}

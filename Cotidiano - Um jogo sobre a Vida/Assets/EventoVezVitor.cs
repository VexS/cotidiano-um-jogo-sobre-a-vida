﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventoVezVitor : MonoBehaviour
{
    public GameObject[] ConversaVitor;

    public static bool JaRespondeu = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (this.name == "CelularADM")
        {
            if (JaRespondeu == true)
            {
                ConversaVitor[0].SetActive(false);
                ConversaVitor[1].SetActive(true);
            }
        }
    }

    public void AcabouAnim()
    {
        Celular.TaConversando = false;
        Celular.RespondeVitor = false;
    }
}
